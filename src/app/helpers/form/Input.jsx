import React from "react";
import { FieldFeedbackLabel } from "./FieldFeedbackLabel";

const getFieldCSSClasses = (touched, errors) => {
  const classes = ["form-control form-control-lg"];
  if (touched && errors) {
    classes.push("is-invalid");
  }

  if (touched && !errors) {
    classes.push("is-valid");
  }

  return classes.join(" ");
};

export function Input({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  label,
  withFeedbackLabel = true,
  customFeedbackLabel,
  type = "text",
  vers = false,
  versData,
  ...props
}) {
  return (
    <>
      {label && (
        <label className="col-xl-3 col-lg-3 col-form-label">{label}</label>
      )}
      <div className="col-lg-9 col-xl-9">
        <input
          type={type}
          className={getFieldCSSClasses(
            touched[field.name],
            errors[field.name]
          )}
          {...field}
          {...props}
        />
        {vers ? (
          <span className="form-text text-muted" style={{ display: "block" }}>
            Versi {versData}
          </span>
        ) : null}
        {withFeedbackLabel && (
          <FieldFeedbackLabel
            error={errors[field.name]}
            touched={touched[field.name]}
            label={label}
            type={type}
            customFeedbackLabel={customFeedbackLabel}
          />
        )}
      </div>
    </>
  );
}

export function InputSubtotal({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  label,
  withFeedbackLabel = true,
  customFeedbackLabel,
  type = "text",
  vers = false,
  versData,
  ...props
}) {
  return (
    <>
      {label && (
        <div className="col-xl-2 col-lg-2" style={{ paddingRight: "0px", paddingLeft: "0px"}}>{label}</div>
      )}
     <div className="col-lg-10 col-xl-10" style={{ paddingRight: "0px"}}>
        <input
          type={type}
          className="form-control form-control-lg"
          {...field}
          {...props}
        />
        {/* {withFeedbackLabel && (
          <FieldFeedbackLabel
            error={errors[field.name]}
            touched={touched[field.name]}
            label={label}
            type={type}
            customFeedbackLabel={customFeedbackLabel}
          />
        )} */}
      </div>
    </>
  );
}

export function InputAtk({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  label,
  withFeedbackLabel = true,
  customFeedbackLabel,
  type = "text",
  vers = false,
  versData,
  ...props
}) {
  return (
    <>
      {label && (
        <label className="col-xl-3 col-lg-3 col-form-label">{label}</label>
      )}
      <div className="col-lg-6 col-xl-6">
        <input
          type={type}
          className={getFieldCSSClasses(
            touched[field.name],
            errors[field.name]
          )}
          {...field}
          {...props}
        />
        {withFeedbackLabel && (
          <FieldFeedbackLabel
            error={errors[field.name]}
            touched={touched[field.name]}
            label={label}
            type={type}
            customFeedbackLabel={customFeedbackLabel}
          />
        )}
      </div>
    </>
  );
}

export function InputNomorPo({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  label,
  withFeedbackLabel = true,
  customFeedbackLabel,
  type = "text",
  vers = false,
  versData,
  ...props
}) {
  return (
    <>
      {label && (
        <label className="col-xl-3 col-lg-3 col-form-label">{label}</label>
      )}
      <div className="col-lg-9 col-xl-9">
        <input
          type={type}
          className="form-control form-control-lg"
          {...field}
          {...props}
        />
        {vers ? (
          <span className="form-text text-muted" style={{ display: "block" }}>
            Versi {versData}
          </span>
        ) : null}
        {withFeedbackLabel && (
          <FieldFeedbackLabel
            error={errors[field.name]}
            touched={touched[field.name]}
            label={label}
            type={type}
            customFeedbackLabel={customFeedbackLabel}
          />
        )}
      </div>
    </>
  );
}