import React from "react";
import { useField, ErrorMessage } from "formik";
import { FieldFeedbackLabel } from "./FieldFeedbackLabel";

const getFieldCSSClasses = (touched, errors) => {
  const classes = ["form-control", "form-control-lg"];
  if (touched && errors) {
    classes.push("is-invalid-select");
  }

  if (touched && !errors) {
    classes.push("is-valid-select");
  }

  return classes.join(" ");
};

export function Select({
  label,
  withFeedbackLabel = true,
  type = "text",
  customFeedbackLabel,
  children,
  custom,
  handleChange,
  ...props
}) {
  const [field, meta] = useField(props);
  const { touched, error } = meta;
  // const renderError = (message) => <p style={{ color: "red" }}>{message}</p>;
  const renderError = (message) => <div className="invalid-feedback" style={{ display: "block" }}>
  {message}
</div>;
  
// console.log(field.name)
  return (
    <>
        <>
          {label && (
            <label className="col-xl-3 col-lg-3 col-form-label">{label}</label>
          )}
          <div className="col-lg-9 col-xl-9">
            <select
              className={getFieldCSSClasses(touched, error)}
              {...field}
              {...props}
            >
              {children}
            </select>
            <ErrorMessage name={field.name} render={renderError} />
            {withFeedbackLabel && (
              <FieldFeedbackLabel
                erros={error}
                touched={touched}
                label={label}
                customFeedbackLabel={customFeedbackLabel}
              />
            )}
          </div>
        </>
    </>
  );
}

export function SelectKasubbag({
  label,
  withFeedbackLabel = true,
  type = "text",
  customFeedbackLabel,
  children,
  custom,
  handleChange,
  ...props
}) {
  const [field, meta] = useField(props);
  const { touched, error } = meta;
  // const renderError = (message) => <p style={{ color: "red" }}>{message}</p>;
  const renderError = (message) => <div className="invalid-feedback" style={{ display: "block" }}>
  {message}
</div>;
  
// console.log(field.name)
  return (
    <>
        <>
          <div className="col-lg-9 col-xl-9">
            <select
              className={getFieldCSSClasses(touched, error)}
              {...field}
              {...props}
            >
              {children}
            </select>
            <ErrorMessage name={field.name} render={renderError} />
            {withFeedbackLabel && (
              <FieldFeedbackLabel
                erros={error}
                touched={touched}
                label={label}
                customFeedbackLabel={customFeedbackLabel}
              />
            )}
          </div>
        </>
    </>
  );
}
