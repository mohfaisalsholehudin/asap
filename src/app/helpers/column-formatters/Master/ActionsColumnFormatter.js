// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { lowerCase } from "lodash";
import { useSelector } from "react-redux";


export function ActionsColumnFormatterMasterRuangan(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    publishKnowledge,
    reviewProposal,
    showReview,
  }
) {


  return (
    <>
      <a
        title="Edit Ruangan"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() =>
          openEditDialog(
            row.id,
          )
        }
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterMasterAtk(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openOpnameDialog,
    openDeleteDialog,
    showProposal,
    publishKnowledge,
    reviewProposal,
    showReview,
  }
) {


  return (
    <>
      <a
        title="Edit ATK"
        className="btn btn-icon btn-light-primary btn-hover-primary btn-sm"
        onClick={() =>
          openEditDialog(
            row.id,
          )
        }
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <> </>
      <a
        title="Perbarui ATK"
        className="btn btn-icon btn-light-warning btn-hover-warning btn-sm mx-3"
        onClick={() =>
          openOpnameDialog(
            row.id
          )
        }
      >
        <span className="svg-icon svg-icon-md svg-icon-warning">
          <i className="fas fa-redo text-warning"></i>
        </span>
      </a>
    </>
  );
}


export function ActionsColumnFormatterMasterKdo(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    publishKnowledge,
    reviewProposal,
    showReview,
  }
) {


  return (
    <>
      <a
        title="Edit KDO"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() =>
          openEditDialog(
            row.id,
          )
        }
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <> </>
{/* 
      <a
        title="Hapus Usulan"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() =>
          openDeleteDialog(
            row.id_dok_senjang
          )
        }
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
      <> </>
      <a
        title="Ajukan Usulan"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() =>
          publishKnowledge(
            row.id_dok_senjang
          )
        }
      >
        <span className="svg-icon svg-icon-md svg-icon-success">
          <i className="fab fa-telegram-plane text-success"></i>
        </span>
      </a> */}
    </>
  );
}


export function ActionsColumnFormatterMasterBmn(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    publishKnowledge,
    reviewProposal,
    showReview,
  }
) {


  return (
    <>
      <a
        title="Edit KDO"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() =>
          openEditDialog(
            row.id,
          )
        }
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <> </>
    </>
  );
}

export function ActionsColumnFormatterMasterAtkDetailPurchase(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>

      <a
        title="Hapus Atk"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}