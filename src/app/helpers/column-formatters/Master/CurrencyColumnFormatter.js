import React from "react";

export function HargaColumnFormatter(cellContent, row) {
  const formatCurrency = (value) => {
    // Check if the input is a valid number
    if (isNaN(value)) {
      return "";
    }

    // Parse the input value to a floating-point number
    const floatValue = parseFloat(value);

    // Format the number as currency (you can adjust this logic based on your requirements)
    const formattedCurrency = floatValue.toLocaleString("id-ID", {
      style: "currency",
      currency: "IDR",
    });

    return formattedCurrency;
  };

    return (
      formatCurrency(row.harga)
    );
  }

export function SubtotalColumnFormatter(cellContent, row) {
  const formatCurrency = (value) => {
    // Check if the input is a valid number
    if (isNaN(value)) {
      return "";
    }

    // Parse the input value to a floating-point number
    const floatValue = parseFloat(value);

    // Format the number as currency (you can adjust this logic based on your requirements)
    const formattedCurrency = floatValue.toLocaleString("id-ID", {
      style: "currency",
      currency: "IDR",
    });

    return formattedCurrency;
  };

    return (
      formatCurrency(row.subTotal)
    );
  }
  