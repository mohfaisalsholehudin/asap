import React from "react";

export function EmailColumnFormatterMasterPengguna(cellContent, row) {

    const getLabelCssClasses = () => {
        return `label label-lg label-light-success label-inline`;
   
      
    };
    return (
      <span className={getLabelCssClasses()}>{row.email}</span>
    );
  }
  