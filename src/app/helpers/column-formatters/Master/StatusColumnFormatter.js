import React from "react";

// export function StatusColumnFormatterMasterRuangan(cellContent, row) {

//     const getLabelCssClasses = () => {
//         return `label label-lg label-light-success label-inline`;
   
      
//     };
//     return (
//       <span className={getLabelCssClasses()}>{row.status}</span>
//     );
//   }
  
  export function StatusColumnFormatterMasterKdo(cellContent, row) {
    const CustomerStatusCssClasses = [
      "success",
      "danger",
      "",
    ];
    const CustomerStatusTitles = [
      "Tersedia",
      "Dipinjam",
      "",
    ];
    let status = "";

    switch (row.ready) {
      case 1:
        status = 0;
        break;
  
      case 0:
        status = 1;
        break;

      default:
        status = 2;
        break;
    }
    const getLabelCssClasses = () => {
      if (row.ready === "") {
        return "";
      } else {
        return `label label-light-${CustomerStatusCssClasses[status]} label-inline`;
      }
    };
    return (
      <>
      <span className={getLabelCssClasses()}>
        {CustomerStatusTitles[status]}
      </span>
    </>
    );
  }

  export function StatusColumnFormatterMasterRuangan(cellContent, row) {
    const CustomerStatusCssClasses = [
      "success",
      "danger",
      "",
    ];
    const CustomerStatusTitles = [
      "Tersedia",
      "Dipinjam",
      "",
    ];
    let status = "";

    switch (row.status) {
      case "Tersedia":
        status = 0;
        break;
  
      case "Dipinjam":
        status = 1;
        break;

      default:
        status = 2;
        break;
    }
    const getLabelCssClasses = () => {
      if (row.status === "") {
        return "";
      } else {
        return `label label-light-${CustomerStatusCssClasses[status]} label-inline`;
      }
    };
    return (
      <>
      <span className={getLabelCssClasses()}>
        {CustomerStatusTitles[status]}
      </span>
    </>
    );
  }

  export function NamaColumnFormatterMasterRuangan(cellContent, row) {

    return (
      <>
      <span>{row.nama}</span>
      <br/>
      <div style={{ fontStyle: "italic"}} className="text-muted">
       PIC: {row.pic}
      </div>
      </>
    );
  }

  export function KapasitasColumnFormatterMasterRuangan(cellContent, row) {

    return (
      <>
      <span>{row.kapasitas} Orang</span>
      </>
    );
  }
  

  export function JabatanColumnFormatterMasterPengguna(cellContent, row) {

    return (
      <>
      <span>{row.jabatan}</span>
      <br/>
      <div style={{ fontStyle: "italic"}} className="text-muted">
       ( {row.unitKerja} )
      </div>
      </>
    );
  }

  export function StatusColumnFormatterKelolaPemeliharaanKdo(cellContent, row) {
    const CustomerStatusCssClasses = [
      "success",
      "warning",
      "danger",
      "",
    ];
    const CustomerStatusTitles = [
      "Terawat",
      "Perlu Perawatan",
      "Segera Perawatan",
      "",
    ];
    let status = "";

    switch (row.status) {
      case "Terawat":
        status = 0;
        break;
  
      case "Perlu Perawatan":
        status = 1;
        break;

      case "Segera Perawatan":
          status = 2;
          break;

      default:
        status = 3;
        break;
    }
    const getLabelCssClasses = () => {
      if (row.status === "") {
        return "";
      } else {
        return `label label-light-${CustomerStatusCssClasses[status]} label-inline`;
      }
    };
    return (
      <>
      <span className={getLabelCssClasses()}>
        {CustomerStatusTitles[status]}
      </span>
    </>
    );
  }
  export function JenisColumnFormatterKelolaPemeliharaanKdo(cellContent, row) {

    return (
      <>
      <span>{row.jenis}</span>
      <br/>
      <div style={{ fontStyle: "italic"}} className="text-muted">
       Tahun: {row.tahun}
      </div>
      {/* <br/> */}
      <div style={{ fontStyle: "italic"}} className="text-muted">
       Nomor Plat: {row.nomorPlat}
      </div>
      </>
    );
  }



  export function StatusColumnFormatterDataArsip(cellContent, row) {
    const CustomerStatusCssClasses = [
      "success",
      "danger",
      "",
    ];
    const CustomerStatusTitles = [
      "Tersedia",
      "Dipinjam",
      "",
    ];
    let status = "";

    switch (row.ready) {
      case 0:
        status = 0;
        break;
  
      case 1:
        status = 1;
        break;

      default:
        status = 2;
        break;
    }
    const getLabelCssClasses = () => {
      if (row.status === "") {
        return "";
      } else {
        return `label label-light-${CustomerStatusCssClasses[status]} label-inline`;
      }
    };
    return (
      <>
      <span className={getLabelCssClasses()}>
        {CustomerStatusTitles[status]}
      </span>
    </>
    );
  }