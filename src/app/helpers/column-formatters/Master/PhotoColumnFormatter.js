import React from "react";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

const {FILE_URL} = window.ENV;

export function PhotoColumnFormatterMasterPengguna(cellContent, row, rowIndex) {
  return (
    <div>
      <img src={toAbsoluteUrl("/media/users/user.png")} alt="" style={{ width: "50%", borderRadius:'5rem' }} />
    </div>
  );
}

export function PhotoColumnFormatterMasterRuangan(cellContent, row, rowIndex) {

 

 
  const checkPhoto = (val) => {
    if(val.slice(0,5).includes('https')){
        return <img src={toAbsoluteUrl(val)} alt="" style={{ width: "100px", borderRadius:'5rem' }} />

    }else{
      return <img src={toAbsoluteUrl(FILE_URL + val)} alt="" style={{ width: "100px", borderRadius:'5rem' }} />
    }
  }
  return (
    <div>
      {checkPhoto(row.photo)}
    </div>
  );
}

export function PhotoColumnFormatterMasterKdo(cellContent, row, rowIndex) {
  return (
    <div>
      <img src={toAbsoluteUrl(row.photo)} alt="" style={{ width: "100px", borderRadius:'5rem' }} />
    </div>
  );
}

export function PhotoColumnFormatterMasterBmn(cellContent, row, rowIndex) {
  return (
    <div>
      <img src={toAbsoluteUrl(row.photo)} alt="" style={{ width: "100px", borderRadius:'5rem' }} />
    </div>
  );
}

export function PhotoColumnFormatterMasterAtk(cellContent, row, rowIndex) {
  return (
    <div>
      <img src={toAbsoluteUrl(row.photo)} alt="" style={{ width: "100px", borderRadius:'5rem' }} />
    </div>
  );
}