// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
export function ActionsColumnFormatterDataArsip(
    cellContent,
    row,
    rowIndex,
    { openEditDialog, openDetailDialog }
  ) {
    return (
      <>
        <a
          title="Edit"
          className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3 my-3"
          onClick={() => openEditDialog(row.id)}
        >
          <span className="svg-icon svg-icon-md svg-icon-primary">
            <i className="fas fa-edit text-primary"></i>
          </span>
        </a>
        <></>
        <a
        title="Open Detail Arsip"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3 my-3"
        onClick={() => openDetailDialog(row.id)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
      </>
    );
  }