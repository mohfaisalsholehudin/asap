import React, { Suspense, useEffect, useState, lazy } from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../_metronic/layout";
import { BuilderPage } from "./pages/BuilderPage";
import { MyPage } from "./pages/MyPage";
import { DashboardPage } from "./pages/DashboardPage";
import axios from 'axios';
import Pengaturan from "./modules/Administrator/pengaturan/Pengaturan";
import Arsip from "./modules/Arsip/Arsip";
import ArsipEdit from "./modules/Arsip/ArsipEdit";
import ArsipOpen from "./modules/Arsip/ArsipOpen";

const MasterPage = lazy(() =>
  import("./modules/Master/Master")
);
const KelolaPage = lazy(() =>
  import("./modules/Kelola/Kelola")
);
const AdministratorPage = lazy(() =>
  import("./modules/Administrator/Administrator")
);

export default function BasePage() {
  // useEffect(() => {
  //   console.log('Base page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/dashboard" />
        }
        <ContentRoute path="/dashboard" component={DashboardPage} />
        <Route path="/admin" component={AdministratorPage} />
        <Route path="/kelola" component={KelolaPage} />
        <Route path="/master" component={MasterPage} />
        <ContentRoute path="/arsip/:id/view" component={ArsipOpen} />
        <ContentRoute path="/arsip/tambah" component={ArsipEdit} />
        <Route path="/arsip" component={Arsip} />

        {/* <ContentRoute path="/builder" component={BuilderPage} /> */}
        <ContentRoute path="/my-page" component={MyPage} />
        {/* <Route path="/google-material" component={GoogleMaterialPage} />
        <Route path="/react-bootstrap" component={ReactBootstrapPage} />
        <Route path="/e-commerce" component={ECommercePage} />
        <Route path="/user-profile" component={UserProfilepage} /> */}
        <Redirect to="error/error-v1" />
      </Switch>
    </Suspense>
  );
}
