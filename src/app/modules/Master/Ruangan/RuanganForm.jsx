import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Input, Select as Sel, Textarea } from "../../../helpers";
import { CustomUploadPhoto } from "../../../helpers/form/CustomUploadPhoto";

function RuanganForm({ content, btnRef, saveForm }) {
  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    namaRuangan: Yup.string()
      .required("Nama Ruangan wajib diisi"),
    kode: Yup.string()
      .required("Kode Ruangan wajib diisi"),
    lokasi: Yup.string()
      .required("Lokasi Ruangan wajib diisi"),
    deskripsi: Yup.string().required("Deskripsi wajib diisi"),
    pic: Yup.string().required("Penanggungjawab wajib diisi"),
    kapasitas: Yup.number().required("Kapasitas wajib diisi"),
    file: Yup.mixed()
      .required("File wajib diisi")
      .test(
        "fileSize",
        "File terlalu besar",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Format file tidak sesuai",
        (value) => value && SUPPORTED_FORMATS.some((a) => value.type.includes(a))
      ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["image/jpeg"];

  // useEffect(() => {
  //   content.status !== "" ? setShow(true):setShow(false)
  //  }, [content]);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
          // console.log(values)
        }}
      >
        {({ handleSubmit }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Kode Ruangan */}
                <div
                  className="form-group row"
                  style={{ marginBottom: "0.5rem" }}
                >
                  <Field
                    name="kode"
                    component={Input}
                    placeholder="Kode"
                    label="Kode"
                  />
                </div>
                <div className="form-group row">
                  <div className="col-xl-3 col-lg-3"></div>
                  <div className="col-lg-9 col-xl-6">
                    <p>
                      Kodefikasi ruangan dengan <b>Lantai + Nomor Ruangan</b>
                    </p>
                  </div>
                </div>

                {/* Field Nama Ruangan */}
                <div className="form-group row">
                  <Field
                    name="namaRuangan"
                    component={Input}
                    placeholder="Nama"
                    label="Nama"
                  />
                </div>
                {/* Field Lokasi Ruangan */}
                <div className="form-group row">
                  <Field
                    name="lokasi"
                    component={Input}
                    placeholder="Lokasi"
                    label="Lokasi"
                  />
                </div>
                 {/* Field Kapasitas Ruangan */}
                 <div className="form-group row">
                  <Field
                    name="kapasitas"
                    component={Input}
                    type="number"
                    placeholder="Kapasitas"
                    label="Kapasitas"
                  />
                </div>
                {/* Field Deskripsi */}
                <div className="form-group row">
                  <Field
                    name="deskripsi"
                    component={Textarea}
                    placeholder="Deskripsi"
                    label="Deskripsi"
                  />
                </div>
                 {/* Field Penanggungjawab Ruangan */}
                 <div className="form-group row">
                  <Field
                    name="pic"
                    component={Input}
                    placeholder="Penanggungjawab"
                    label="Penanggungjawab"
                  />
                </div>
                {/* Field Unggah Foto */}

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unggah Foto
                  </label>
                  <div className="col-lg-9 col-xl-9">
                    <Field
                      name="file"
                      component={CustomUploadPhoto}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RuanganForm;
