import React, { useState } from "react";
import { Field, Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Input, Select as Sel, Textarea } from "../../../helpers";
import { CustomUploadPhoto } from "../../../helpers/form/CustomUploadPhoto";

function KdoForm({ content, btnRef, saveForm }) {
  const [thisYear, setThisYear] = useState(new Date().getFullYear());
  const minOffset = 0;
  const maxOffset = 20;
  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    jenis: Yup.string()
      .required("Jenis wajib diisi"),
    tahun: Yup.string().required("Tahun wajib diisi"),
    nomorPlat: Yup.string()
      .required("No. Plat wajib diisi"),
    deskripsi: Yup.string().required("Deskripsi wajib diisi"),
    file: Yup.mixed()
      .required("File wajib diisi")
      .test(
        "fileSize",
        "File terlalu besar",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Format file tidak sesuai",
        (value) => value && SUPPORTED_FORMATS.some((a) => value.type.includes(a))

      ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["image/jpeg"];


  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
        }}
      >
        {({ handleSubmit }) => {
          const options = [];
          for (let i = minOffset; i <= maxOffset; i++) {
            const year = thisYear - i;
            options.push(
              <option key={year} value={year}>
                {year}
              </option>
            );
          }
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Jenis */}
                <div
                  className="form-group row"
                  // style={{ marginBottom: "0.5rem" }}
                >
                  <Field
                    name="jenis"
                    component={Input}
                    placeholder="Jenis"
                    label="Jenis"
                  />
                </div>
                {/* Field Tahun */}
                <div className="form-group row">
                  <Sel name="tahun" label="Tahun">
                    <option>--Pilih Tahun--</option>
                    {options}
                  </Sel>
                </div>
                {/* <div className="form-group row">
                  <div className="col-lg-3 col-xl-3">

                  </div>
                  <div className="col-lg-9 col-xl-9">
                  <ErrorMessage name="tahun" render={renderError} />

                  </div>
                </div> */}

                {/* Field Nomor Plat */}
                <div className="form-group row">
                  <Field
                    name="nomorPlat"
                    component={Input}
                    placeholder="Nomor Plat"
                    label="Nomor Plat"
                  />
                </div>
                {/* Field Deskripsi */}
                <div className="form-group row">
                  <Field
                    name="deskripsi"
                    component={Textarea}
                    placeholder="Deskripsi"
                    label="Deskripsi"
                  />
                </div>
                {/* Field Unggah Foto */}

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unggah Foto
                  </label>
                  <div className="col-lg-9 col-xl-9">
                    <Field
                      name="file"
                      component={CustomUploadPhoto}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default KdoForm;
