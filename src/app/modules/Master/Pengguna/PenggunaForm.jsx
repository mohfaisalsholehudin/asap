import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Input, Select as Sel } from "../../../helpers";

function PenggunaForm({ content, btnRef, saveForm }) {
  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    email: Yup.string()
      .email("Harap masukkan format email yang sesuai")
      .min(3, "Minimal 3 karakter")
      .max(50, "Maksimal 50 karakter")
      .required("Email wajib diisi"),
    ip: Yup.number().required("IP wajib diisi"),
    role: Yup.string().required("Level / Role wajib diisi"),
    nama: Yup.string().required("Nama wajib diisi"),
    unitKerja: Yup.string().required("Unit Kerja wajib diisi"),
    jabatan: Yup.string().required("Jabatan wajib diisi"),
  });

  // useEffect(() => {
  //   content.status !== "" ? setShow(true):setShow(false)
  //  }, [content]);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
        }}
      >
        {({ handleSubmit }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field NIP */}
                <div className="form-group row">
                  <Field
                    name="ip"
                    component={Input}
                    placeholder="IP"
                    label="IP"
                  />
                </div>
                {/* Field Nama */}
                <div className="form-group row">
                  <Field
                    name="nama"
                    component={Input}
                    placeholder="Nama"
                    label="Nama"
                  />
                </div>
                {/* Field Unit Kerja */}
                <div className="form-group row">
                  <Field
                    name="unitKerja"
                    component={Input}
                    placeholder="Unit Kerja"
                    label="Unit Kerja"
                  />
                </div>
                {/* Field Jabatan */}
                <div className="form-group row">
                  <Field
                    name="jabatan"
                    component={Input}
                    placeholder="Jabatan"
                    label="Jabatan"
                  />
                </div>
                {/* Field Email */}
                <div className="form-group row">
                  <Field
                    name="email"
                    component={Input}
                    placeholder="Email@mail.com"
                    label="Email"
                  />
                </div>

                {/* Field Role */}
                <div className="form-group row">
                  <Sel name="role" label="Level / Role">
                    <option value="1">Admin</option>
                    <option value="2">User</option>
                  </Sel>
                </div>
                <div className="form-group row">
                  <div className="col-xl-3 col-lg-3"></div>
                  <div
                    className="col-lg-9 col-xl-9"
                    style={{
                      paddingTop: "10px",
                      color: "#f64e60",
                      backgroundColor: "#ffe2e5",
                      borderRadius: "1rem",
                    }}
                  >
                    <h4>Perhatian</h4>
                    <p>Email dan IP Pegawai tidak bisa dirubah.</p>
                    <p>
                      {" "}
                      User baru akan menggunakan email dan IP sebagai password
                      untuk login, mintalah untuk segera merubah password
                      melalui aplikasi.{" "}
                    </p>
                  </div>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PenggunaForm;
