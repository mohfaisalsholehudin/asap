import React,  { Suspense }  from "react";
import {Redirect, Switch } from "react-router-dom";
import {LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";

/* Begin Pengguna */
import Pengguna from "./Pengguna/Pengguna";
import PenggunaEdit from "./Pengguna/PenggunaEdit";
import Ruangan from "./Ruangan/Ruangan";
import RuanganEdit from "./Ruangan/RuanganEdit";
import Atk from "./Atk/Atk";
import Pembelian from "./Atk/Pembelian/Pembelian";
import PembelianTambah from "./Atk/Pembelian/Tambah/PembelianTambah";
import Kdo from "./Kdo/Kdo";
import KdoEdit from "./Kdo/KdoEdit";
import Bmn from "./Bmn/Bmn";
import BmnEdit from "./Bmn/BmnEdit";
import AtkEdit from "./Atk/AtkEdit";
import AtkOpname from "./Atk/AtkOpname";
/* Begin Pengguna */

export default function Master() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
    <Switch>
         {
          /* Redirect from eCommerce root URL to /Pengajuan Usulan Matrix */
          <Redirect
            exact={true}
            from="/master"
            to="/master/pengguna"
          />
        }
        {/* Begin Pengguna */}
            <ContentRoute path="/master/pengguna/tambah" component={PenggunaEdit} />
            <ContentRoute path="/master/pengguna" component={Pengguna} />
        {/* End of Pengguna */}
        {/* Begin Ruangan */}
            <ContentRoute path="/master/ruangan/:id/edit" component={RuanganEdit} />
            <ContentRoute path="/master/ruangan/tambah" component={RuanganEdit} />
            <ContentRoute path="/master/ruangan" component={Ruangan} />
        {/* End of Ruangan */}
        {/* Begin ATK */}
            <ContentRoute path="/master/atk/pembelian/tambah" component={PembelianTambah} />
            <ContentRoute path="/master/atk/pembelian" component={Pembelian} />
            <ContentRoute path="/master/atk/list/:id/opname" component={AtkOpname} />
            <ContentRoute path="/master/atk/list/:id/edit" component={AtkEdit} />
            <ContentRoute path="/master/atk/list/tambah" component={AtkEdit} />
            <ContentRoute path="/master/atk" component={Atk} />
        {/* End of ATK */}
        {/* Begin KDO */}
            <ContentRoute path="/master/kdo/:id/edit" component={KdoEdit} />
            <ContentRoute path="/master/kdo/tambah" component={KdoEdit} />
            <ContentRoute path="/master/kdo" component={Kdo} />
        {/* End of KDO */}
        {/* Begin BMN */}
          <ContentRoute path="/master/bmn/:id/edit" component={BmnEdit} />
          <ContentRoute path="/master/bmn/tambah" component={BmnEdit} />
          <ContentRoute path="/master/bmn" component={Bmn} />
        {/* End of BMN */}


    </Switch>
  </Suspense>

  );
}
