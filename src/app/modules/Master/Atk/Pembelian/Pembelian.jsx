/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";

import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";

import PembelianTable from "./PembelianTable";
import PembelianTambah from "./Tambah/PembelianTambah";
import PembelianBaru from "./PembelianBaru";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import PembelianPencarian from "./PembelianPencarian";
import PembelianPencarianTable from "./PembelianPencarianTable";
import { checkAtkEmpty } from "../../../references/Api";

function Pembelian() {
  const [show, setShow] = useState(false);
  const [noPo, setNoPo] = useState("");
  const [content, setContent] = useState();
  const [atkButton, setAtkButton] = useState(false)

  useEffect(()=>{
    checkAtkEmpty().then(({data})=> {
      if(data.length > 0){
        setContent({
          nomorPo: data[0].nomorPo,
          tglPembelian: data[0].tglPembelian
        })
      }
      
      data.length > 0 ? setAtkButton(true) : setAtkButton(false)
    })
  },[])

  return (
    <>
      {/* <PembelianTambah /> */}

      <div className="row">
        <div className="col-lg-6 col-md-6">
          <Card style={{ height: "450px" }}>
            <CardHeader
              title="Pembelian ATK"
              style={{ backgroundColor: "#FFC91B" }}
            ></CardHeader>
            <CardBody>
              <PembelianBaru
                atkButton={atkButton}
                content={content}
              />
            </CardBody>
          </Card>
        </div>
        <div className="col-lg-6 col-md-6">
          <Card style={{ height: "450px" }}>
            <CardHeader
              title="Pencarian ATK"
              style={{ backgroundColor: "#FFC91B" }}
            ></CardHeader>
            <CardBody>
              <PembelianPencarian
                setShow={setShow}
                setNoPo={setNoPo}
              />
            </CardBody>
          </Card>
        </div>
      </div>
      {show ? 
      <div className="row">
        <div className="col-lg-12 col-md-12">
          <Card>
            <CardHeader
              title="Data Pembelian"
              style={{ backgroundColor: "#FFC91B" }}
            ></CardHeader>
            <CardBody>
              <PembelianPencarianTable
              noPo={noPo}
              />
            </CardBody>
          </Card>
        </div>
      </div>
      :
      null }
    </>
  );
}

//#a6c8e6
export default Pembelian;
