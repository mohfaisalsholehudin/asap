import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import SVG from "react-inlinesvg";

import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../../helpers/column-formatters";
import {
  cancelPurchaseAtk,
  checkAtkEmpty,
  deleteDetailPurchaseAtk,
  savePurchaseAtk,
} from "../../../../references/Api";
function PembelianTambahTable({ id }) {
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const [content, setContent] = useState([]);
  const history = useHistory();

  useEffect(() => {
    checkAtkEmpty().then(({ data }) => {
      if (data.length > 0) {
        data[0] ? setContent(data[0].atkPurchaseDetails) : setContent(data[0]);
      }
    });
  }, []);

  const deleteAction = (id) => {
    deleteDetailPurchaseAtk(id).then(({ status }) => {
      if (status === 200) {
        history.push("/dashboard");
        history.replace("/master/atk/pembelian/tambah");
      } else {
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.replace("/master/atk/pembelian/tambah");
        });
      }
    });
  };
  const priceFormatter = (column, colIndex, { text }) => {
    if (isNaN(text)) {
      return "";
    }

    // Parse the input value to a floating-point number
    const floatValue = parseFloat(text);

    // Format the number as currency (you can adjust this logic based on your requirements)
    const formattedCurrency = floatValue.toLocaleString("id-ID", {
      style: "currency",
      currency: "IDR",
    });

    return formattedCurrency;
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      hidden: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        width: "100px",
      },
    },
    {
      dataField: "id",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "atk.kode",
      text: "Kode",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      footer: "TOTAL",
      //   style: {
      //     width: "30px",
      //   },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "atk.namaAtk",
      text: "Nama",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      footer: "",

      //   style: {
      //     width: "50px",
      //   },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "jumlah",
      text: "Qty",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      footer: "",
    },
    {
      dataField: "harga",
      text: "Harga",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      footer: "",
      formatter: columnFormatters.HargaColumnFormatter,
    },
    {
      dataField: "subTotal",
      text: "Subtotal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      footer: (columnData) =>
        columnData.reduce((acc, item) => acc + parseInt(item), 0),
      formatter: columnFormatters.SubtotalColumnFormatter,
      footerFormatter: priceFormatter,
    },
    // {
    //   dataField: "stock",
    //   text: "Stock",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   formatter: columnFormatters.StockColumnFormatterMasterAtk,
    //   style: {
    //     width: "80px",
    //   },
    // },
    // {
    //   dataField: "status",
    //   text: "Status",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   hidden: tab === 'selesai' || tab === 'tolak' ? true : false,
    //   formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeUsulanMatrix,
    // },
    {
      dataField: "action",
      text: "Aksi",
      footer: "",
      formatter: columnFormatters.ActionsColumnFormatterMasterAtkDetailPurchase,
      formatExtraData: {
        // openEditDialog: edit,
        openDeleteDialog: deleteAction,
        // publishKnowledge: apply,
        // showReview: review,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "70px",
      },
    },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "id", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };

  const handleCancelPurchaseAtk = () => {
    cancelPurchaseAtk(id).then(({ status }) => {
      if (status === 200) {
        swal("Berhasil", "Pembelian berhasil dibatalkan", "success").then(
          () => {
            history.push("/dashboard");
            history.replace(`/master/atk/pembelian`);
          }
        );
      } else {
        swal("Gagal", "Pembelian gagal dibatalkan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/master/atk/pembelian/tambah`);
        });
      }
    });
  };
  const handleSavePurchaseAtk = () => {
    savePurchaseAtk(id).then(({ status }) => {
      if (status === 200) {
        swal("Berhasil", "Pembelian berhasil disimpan", "success").then(() => {
          history.push("/dashboard");
          history.replace(`/master/atk/pembelian`);
        });
      } else {
        swal("Gagal", "Pembelian gagal disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/master/atk/pembelian/tambah`);
        });
      }
    });
  };

  return (
    <div className="card card-custom card-stretch">
      {/* begin::Form */}
      <div className="form">
        {/* begin::Body */}
        <div className="card-body" style={{ paddingTop: "0px" }}>
          <div className="row" style={{ display: "block" }}>
            <label className="col-xl-3"></label>
            <div className="col-lg-9 col-xl-6" style={{ paddingLeft: "0px" }}>
              <h5 className="font-weight-bold mb-6">DAFTAR ITEM</h5>
            </div>
          </div>
          <div className="row">
            <>
              <>
                <PaginationProvider pagination={paginationFactory(pagiOptions)}>
                  {({ paginationProps, paginationTableProps }) => {
                    return (
                      <>
                        <ToolkitProvider
                          keyField="id"
                          data={content}
                          columns={columns}
                          search
                        >
                          {(props) => (
                            <div
                              className="col-lg-12"
                              style={{
                                paddingRight: "0px",
                                paddingLeft: "0px",
                              }}
                            >
                              <BootstrapTable
                                {...props.baseProps}
                                wrapperClasses="table-responsive"
                                bordered={false}
                                headerWrapperClasses="thead-light"
                                classes="table table-condensed table-head-custom table-vertical-center overflow-hidden"
                                defaultSorted={defaultSorted}
                                bootstrap4
                                {...paginationTableProps}
                              ></BootstrapTable>
                              {/* <Pagination
                        paginationProps={paginationProps}
                        //   isLoading={loading}
                      /> */}
                            </div>
                          )}
                        </ToolkitProvider>
                      </>
                    );
                  }}
                </PaginationProvider>
              </>
            </>
          </div>
          <div
            className="row mt-3"
            style={{ display: "block", textAlign: "right" }}
          >
            <button
              type="submit"
              className="btn btn-danger"
              onClick={() => handleCancelPurchaseAtk()}
              disabled={false}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                // paddingRight: "0px", paddingLeft: "0px"
              }}
              // disabled={disabled}
            >
              <i className="far fa-times-circle"></i>
              Batalkan
            </button>
            {content.length > 0 ? (
              <button
                type="submit"
                className="btn btn-success ml-2"
                onClick={() => handleSavePurchaseAtk()}
                disabled={false}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  // paddingRight: "0px", paddingLeft: "0px"
                }}
                // disabled={disabled}
              >
                <i className="fas fa-save"></i>
                Simpan
              </button>
            ) : (
              <button
                type="submit"
                className="btn btn-success ml-2"
                onClick={() => handleSavePurchaseAtk()}
                disabled={true}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  // paddingRight: "0px", paddingLeft: "0px"
                }}
                // disabled={disabled}
              >
                <i className="fas fa-save"></i>
                Simpan
              </button>
            )}
          </div>
        </div>
        {/* end::Body */}
      </div>
      {/* end::Form */}
    </div>
  );
}

export default PembelianTambahTable;
