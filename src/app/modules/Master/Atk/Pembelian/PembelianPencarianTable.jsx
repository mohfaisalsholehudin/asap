import React, { useState, useEffect } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import SVG from "react-inlinesvg";

import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { getPenggunaById, searchPurchase } from "../../../references/Api";



function PembelianPencarianTable({noPo}) {
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const [pengguna, setPengguna] = useState([])
  const [data, setData] = useState([])
  const [content, setContent] = useState([]);


  useEffect(()=> {
    searchPurchase(noPo).then(({data})=> {
      // console.log(data)
      setData(data)
      setContent(data.atkPurchaseDetails)
      getPenggunaById(data.userId).then(({data})=> {
        setPengguna(data)
      })
    })
  },[noPo])

  // const content = [
  //   {
  //     id: "1",
  //     kode_atk: "1100111",
  //     nama_atk: "Spidol White Board",
  //     qty: 4,
  //     harga: 20000,
  //     subtotal: 80000,
  //   },
  //   {
  //     id: "2",
  //     kode_atk: "1100222",
  //     nama_atk: "Drum Cartridge Docu",
  //     qty: 5,
  //     harga: 20000,
  //     subtotal: 100000,
  //   },
  //   {
  //     id: "3",
  //     kode_atk: "1100333",
  //     nama_atk: "Pensil Hitam",
  //     qty: 100,
  //     harga: 10000,
  //     subtotal: 1000000,
  //   },
  // ];
  // const totalPrice = content.reduce((acc, item) => acc + item.subtotal, 0);

  const priceFormatter = (column, colIndex, { text }) => {
    if (isNaN(text)) {
      return "";
    }

    // Parse the input value to a floating-point number
    const floatValue = parseFloat(text);

    // Format the number as currency (you can adjust this logic based on your requirements)
    const formattedCurrency = floatValue.toLocaleString("id-ID", {
      style: "currency",
      currency: "IDR",
    });

    //   console.log(formattedCurrency)

    return formattedCurrency;
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      hidden: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        width: "100px",
      },
    },
    {
      dataField: "id",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "atk.kode",
      text: "Kode",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      footer: "Total (Rp)",
      //   style: {
      //     width: "30px",
      //   },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "atk.namaAtk",
      text: "Nama",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      footer: "",

      //   style: {
      //     width: "50px",
      //   },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "jumlah",
      text: "Jumlah",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      footer: "",
    },
    {
      dataField: "harga",
      text: "Harga (Rp)",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      footer: "",
      formatter: columnFormatters.HargaColumnFormatter,
    },
    {
      dataField: "subTotal",
      text: "Subtotal (Rp)",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      footer: (columnData) => columnData.reduce((acc, item) => acc + parseInt(item), 0),
      formatter: columnFormatters.SubtotalColumnFormatter,
      footerFormatter: priceFormatter,
    },
    // {
    //   dataField: "stock",
    //   text: "Stock",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   formatter: columnFormatters.StockColumnFormatterMasterAtk,
    //   style: {
    //     width: "80px",
    //   },
    // },
    // {
    //   dataField: "status",
    //   text: "Status",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   hidden: tab === 'selesai' || tab === 'tolak' ? true : false,
    //   formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeUsulanMatrix,
    // },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "id", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  //   const rowsWithSubtotal = [...content, { id: 'Subtotal', kode_atk:'TOTAL',
  //   subtotal: totalPrice, }];
  // const rowStyle = { backgroundColor: '#3F4254' };

  const dateFormat = (val) => {
    const date = new Date(val);
    const tanggal = String(date.getDate()).padStart(2, '0');
    var bulan = String(date.getMonth() + 1).padStart(2, '0');
    const tahun = date.getFullYear();
    return tanggal + "-" + bulan + "-" + tahun ;
  }

  return (
    <>
        <div className="py-5">
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <h5 className="font-weight-bold">PEMBELIAN</h5>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <span className="font-weight-normal mr-2">No. PO:</span>
                  <span className="text-muted text-hover-primary">{data.nomorPo}</span>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <span className="font-weight-normal mr-2">Tanggal:</span>
                  <span className="text-muted">{dateFormat(data.tglPembelian)}</span>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <span className="font-weight-normal mr-2">Dibuat Oleh:</span>
                  <span className="text-muted">{pengguna.name}</span>
                </div>
                <div className="d-flex align-items-center justify-content-between">
                  <span className="font-weight-normal mr-2">Pada:</span>
                  <span className="text-muted">{dateFormat(data.createdAt)}</span>
                </div>
              </div>
              <div className="d-flex align-items-center justify-content-between mb-2">
                <h5 className="font-weight-bold">DAFTAR ITEM</h5>
              </div>
              <div className="row">
                <>
                  <>
                    <PaginationProvider
                      pagination={paginationFactory(pagiOptions)}
                    >
                      {({ paginationProps, paginationTableProps }) => {
                        return (
                          <>
                            <ToolkitProvider
                              keyField="id"
                              data={content}
                              columns={columns}
                              search
                            >
                              {(props) => (
                                <div
                                  className="col-lg-12"
                                  style={{
                                    paddingRight: "0px",
                                    paddingLeft: "0px",
                                  }}
                                >
                                  <BootstrapTable
                                    {...props.baseProps}
                                    wrapperClasses="table-responsive"
                                    bordered={false}
                                    headerWrapperClasses="thead-light"
                                    classes="table table-condensed table-head-custom table-vertical-center overflow-hidden table-light"
                                    defaultSorted={defaultSorted}
                                    bootstrap4
                                    // rowStyle={ rowStyle }
                                    {...paginationTableProps}
                                  ></BootstrapTable>
                                </div>
                              )}
                            </ToolkitProvider>
                          </>
                        );
                      }}
                    </PaginationProvider>
                  </>
                </>
              </div>
    </>
  );
}

export default PembelianPencarianTable;
