import React from "react";
import swal from "sweetalert"
import { useHistory } from "react-router-dom";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Input, DatePickerField } from "../../../../helpers";
import { createPurchase } from "../../../references/Api";

function PembelianBaru({ atkButton, content }) {
  const history = useHistory();

  const initValues = {
    tglPembelian: "",
    nomorPo: "",
  };
  const ValidateSchema = Yup.object().shape({
    nomorPo: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nomor PO wajib diisi"),
      tglPembelian: Yup.mixed()
      .nullable(false)
      .required("Tanggal Pembelian wajib diisi"),
  });

  const saveForm = (val) => {
    // console.log(val);
    var localDate = new Date(val.tglPembelian).toISOString().slice(0, 10);
    createPurchase(val.nomorPo, localDate).then(({status})=> {
      if(status === 200){
        history.push("/master/atk/pembelian/tambah");
      }else{
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.replace("/master/atk/pembelian");
        });
      }
    })
  };

  const lanjutkanPembelian = () => {
    history.push("/master/atk/pembelian/tambah");
  }

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content || initValues}
        validationSchema={ValidateSchema}
        onSubmit={(values) => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid,
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Nomor PO */}
                {atkButton ? (
                  <div className="form-group row">
                    <Field
                      name="nomorPo"
                      component={Input}
                      placeholder="Nomor PO"
                      label="Nomor PO"
                      disabled
                      // maxLength={9}
                      // withFeedbackLabel={false}
                    />
                  </div>
                ) : (
                  <div className="form-group row">
                    <Field
                      name="nomorPo"
                      component={Input}
                      placeholder="Nomor PO"
                      label="Nomor PO"
                      // maxLength={9}
                      // withFeedbackLabel={false}
                    />
                  </div>
                )}
                {/* Field Tanggal */}
                {atkButton ? (
                  <div className="form-group row">
                    <DatePickerField
                      name="tglPembelian"
                      label="Tanggal Pembelian"
                      disabled
                    />
                  </div>
                ) : (
                  <div className="form-group row">
                    <DatePickerField name="tglPembelian" label="Tanggal Pembelian" />
                  </div>
                )}
                <div
                  className="form-group row"
                  style={{ marginBottom: "6.3rem" }}
                ></div>
                {atkButton ? (
                  <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => lanjutkanPembelian()}
                    disabled={false}
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                    // disabled={disabled}
                  >
                    <i className="fas fa-shopping-cart"></i>
                    Lanjutkan Pembelian
                  </button>
                ) : (
                  <button
                    type="submit"
                    className="btn btn-success"
                    onSubmit={() => handleSubmit()}
                    disabled={false}
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                    // disabled={disabled}
                  >
                    <i className="fas fa-save"></i>
                    Simpan
                  </button>
                )}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PembelianBaru;
