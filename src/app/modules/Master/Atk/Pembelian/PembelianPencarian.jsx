import React from "react";
import swal from "sweetalert"
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { InputNomorPo } from "../../../../helpers";
import { searchPurchase } from "../../../references/Api";

function PembelianPencarian({setShow, setNoPo}) {
  const initValues = {
    nomorPo: "",
  };
  const ValidateSchema = Yup.object().shape({
    // nomorPo: Yup.string()
    //   .min(2, "Minimum 2 symbols")
    //   .max(50, "Maximum 50 symbols")
    //   .required("Nomor PO wajib diisi"),
  });

  const saveForm = (val) => {
    if(val.nomorPo.length === 0) {
      swal({
        title: "Harap masukkan Nomor PO!",
        text: "Klik OK untuk melanjutkan",
        icon: "info",
        closeOnClickOutside: false,
      }).then((willApply) => {
        if (willApply) {
          // history.push("/logout");
        }
      });
    } else {
      setNoPo(val.nomorPo)
      searchPurchase(val.nomorPo).then(({data})=> {
        if(data){
          setShow(true)
        } 
      }).catch((err)=> {
        swal({
          title: "Nomor PO tidak tersedia!",
          icon: "warning",
          closeOnClickOutside: false,
        }).then((willApply) => {
          if (willApply) {
            // history.push("/logout");
          }
        });
      })

    }
  };
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={ValidateSchema}
        onSubmit={(values) => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid,
        }) => {
          const handleReset = () => {
            setFieldValue("nomorPo", "")
            setShow(false)
            setNoPo("")
          }
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Nomor PO */}
                <div className="form-group row">
                  <Field
                    name="nomorPo"
                    component={InputNomorPo}
                    placeholder="Nomor PO"
                    label="Nomor PO"
                    // maxLength={9}
                    withFeedbackLabel={false}
                  />
                </div>
                <div
                  className="form-group row"
                  style={{ marginBottom: "12.3rem" }}
                ></div>
                <button
                  type="submit"
                  className="btn btn-success"
                  onSubmit={() => handleSubmit()}
                  disabled={false}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="fas fa-search"></i>
                  Cari Pembelian
                </button>
                <button
                  type="button"
                  className="btn btn-warning ml-2"
                  onClick={() => handleReset()}
                  disabled={false}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="fas fa-redo"></i>
                  Reset
                </button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PembelianPencarian;
