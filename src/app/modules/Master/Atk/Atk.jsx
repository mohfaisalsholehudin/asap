/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { useHistory } from "react-router-dom";
import SVG from "react-inlinesvg";

import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

import AtkTable from "./AtkTable";

function Atk() {
  const history = useHistory();

  const add = () => history.push("/master/atk/list/tambah");
  const opname = () => history.push("/master/atk/list/opname");

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Alat Tulis Kantor"
          style={{ backgroundColor: "#FFC91B" }}
        >
          <CardHeaderToolbar>
            <button
              type="button"
              className="btn btn-light-primary ml-2"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                float: "right",
              }}
              onClick={opname}
            >
              <span className="svg-icon menu-icon">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Communication/Outgoing-box.svg")} />
              </span>
              Opname
            </button>
            <button
              type="button"
              className="btn btn-primary ml-2"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                float: "right",
              }}
              onClick={add}
            >
              <span className="svg-icon menu-icon">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Plus.svg")} />
              </span>
              Tambah
            </button>
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <AtkTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Atk;
