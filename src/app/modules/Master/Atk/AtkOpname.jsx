/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Input } from "../../../helpers";
import { getAtkById, opnameAtk } from "../../references/Api";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
function AtkOpname({
  history,
  match: {
    params: { id },
  },
}) {
  const [isDisabled, setIsDisabled] = useState();
  const [loading, setLoading] = useState(false);
  const [initValues, setInitValues] = useState({
    stock: "",
    jumlah: "",
  });
  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    jumlah: Yup.string().required("Jumlah wajib diisi"),
  });

  useEffect(() => {
    getAtkById(id).then(({ data }) => {
      setInitValues({
        stock: data.stock,
        jumlah: "",
      });
    });
  }, []);

  const backAction = () => {
    history.push("/master/atk/list");
  };
  const enableLoading = () => {
    setLoading(true);
    setIsDisabled(true);
  };

  const disableLoading = () => {
    setLoading(false);
    setIsDisabled(false);
  };

  const saveForm = (val) => {
    enableLoading();
    opnameAtk(id, val.jumlah).then(({ status }) => {
      disableLoading();
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
          history.push("/master/atk/list");
        });
      } else {
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.push("/master/atk/list");
        });
      }
    });
  };
  return (
    <>
      <Card>
        <CardHeader
          title="ATK Opname"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <div className="mt-5">
              <Formik
                enableReinitialize={true}
                initialValues={initValues}
                validationSchema={ProposalEditSchema}
                onSubmit={(values) => {
                  saveForm(values);
                  // console.log(values)
                }}
              >
                {({ handleSubmit, setFieldValue, values }) => {
                  return (
                    <>
                      <Form className="form form-label-right">
                        {/* Field Jumlah Saat Ini */}
                        <div className="form-group row">
                          <Field
                            name="stock"
                            component={Input}
                            placeholder="Jumlah Saat Ini"
                            label="Jumlah Saat Ini"
                            type="number"
                            disabled
                          />
                        </div>

                        {/* Field Jumlah Real */}
                        <div className="form-group row">
                          <Field
                            name="jumlah"
                            component={Input}
                            placeholder="Jumlah Real"
                            type="number"
                            label="Jumlah Real"
                          />
                        </div>

                        <div className="form-group row">
                          <div className="col-xl-3 col-lg-3"></div>
                          <div
                            className="col-lg-9 col-xl-9"
                            style={{
                              paddingTop: "10px",
                              color: "#f64e60",
                              backgroundColor: "#ffe2e5",
                              borderRadius: "1rem",
                            }}
                          >
                            <p>
                              {" "}
                              Sistem akan mencatat tindakan ini, pastikan anda
                              telah menuliskan data dengan benar.{" "}
                            </p>
                          </div>
                        </div>

                        {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
                      </Form>
                      <div className="col-lg-12" style={{ textAlign: "right" }}>
                        {loading ? (
                          <button
                            type="button"
                            className="btn btn-light ml-2"
                            onClick={backAction}
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                            }}
                            disabled={isDisabled}
                          >
                            <i className="fa fa-arrow-left"></i>
                            Kembali
                          </button>
                        ) : (
                          <button
                            type="button"
                            onClick={backAction}
                            className="btn btn-light"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                            }}
                          >
                            <i className="fa fa-arrow-left"></i>
                            Kembali
                          </button>
                        )}
                        {`  `}
                        {loading ? (
                          <button
                            type="submit"
                            className="btn btn-success spinner spinner-white spinner-left ml-2"
                            onClick={saveForm}
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                            }}
                            disabled={isDisabled}
                          >
                            <span>Simpan</span>
                          </button>
                        ) : (
                          <button
                            type="submit"
                            className="btn btn-success ml-2"
                            onClick={() => saveForm(values)}
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                            }}
                            // disabled={disabled}
                          >
                            <i className="fas fa-save"></i>
                            Simpan
                          </button>
                        )}
                      </div>
                    </>
                  );
                }}
              </Formik>
            </div>
          </>
        </CardBody>
      </Card>
    </>
  );
}

export default AtkOpname;
