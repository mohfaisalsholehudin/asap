/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";
/* Helper */
import { useSubheader } from "../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../_metronic/_partials/controls";
import AtkForm from "./AtkForm";
import { getAtkById, tambahAtk, updateAtk, uploadFile } from "../../references/Api";

function AtkEdit({
  history,
  match: {
    params: { id },
  },
}) {
  const initValues = {
    kode: "",
    namaAtk: "",
    deskripsi: "",
    file: "",
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();
  const [isDisabled, setIsDisabled] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    let _title = id ? "Edit ATK" : "Tambah ATK";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      getAtkById(id).then(({ data }) => {
        setContent({
          kode: data.kode,
          namaAtk: data.namaAtk,
          deskripsi: data.deskripsi,
          file_upload: data.photo,
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push("/master/atk/list");
  };
  const enableLoading = () => {
    setLoading(true);
    setIsDisabled(true);
  };

  const disableLoading = () => {
    setLoading(false);
    setIsDisabled(false);
  };
  const saveForm = (values) => {
    if (!id) {
      enableLoading(true);
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFile(formData).then(({ data }) => {
        disableLoading();
        tambahAtk(
          values.kode.toString(),
          values.namaAtk,
          values.deskripsi,
          data
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/master/atk/list");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/master/atk/list/tambah");
            });
          }
        });
      });
    } else {
      if (values.file.name) {
        enableLoading();
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData).then(({ data }) => {
          disableLoading();
          updateAtk(
            id,
            values.kode.toString(),
            values.namaAtk,
            values.deskripsi,
            data
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/master/atk/list");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/master/atk/list/tambah");
              });
            }
          });
        });
      } else {
        updateAtk(
          id,
          values.kode.toString(),
          values.namaAtk,
          values.deskripsi,
          values.file_upload
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/master/atk/list");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/master/atk/list/tambah");
            });
          }
        });
      }
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <AtkForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          {loading ? (
            <button
              type="button"
              className="btn btn-light ml-2"
              onClick={backAction}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              disabled={isDisabled}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
          ) : (
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
          )}
          {`  `}
          {loading ? (
            <button
              type="submit"
              className="btn btn-success spinner spinner-white spinner-left ml-2"
              onClick={saveButton}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              disabled={isDisabled}
            >
              <span>Simpan</span>
            </button>
          ) : (
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveButton}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              // disabled={disabled}
            >
              <i className="fas fa-save"></i>
              Simpan
            </button>
          )}
        </div>
      </CardFooter>
    </Card>
  );
}

export default AtkEdit;
