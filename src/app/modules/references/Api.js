import axios from "axios";


export function uploadFile(formData) {
  const config = {
    headers: {
      "Content-type": "multipart/form-data",
    },
  };
  return axios.post(`/api/v1/files`, formData, config);
}

export function getFile(filename){
  return axios.get(`/api/v1/files/${filename}`)
}

export function getPengguna(page = 1, size = 10, sort = "id", order = "asc", search) {
  return axios.get(
    `/api/v1/users?page=${page}&size=${size}&sort=${sort}&order=${order}&search=${search}`
  );
}

export function getPenggunaById(id){
  return axios.get(`/api/v1/users/${id}`)
}

export function savePengguna(email, ipPegawai, role, sectionId, subSectionId){
    return axios.post(`/api/v1/users`,{
        email,
        ipPegawai,
        role,
        sectionId,
        subSectionId
    })
}

export function getRuangan(page = 1, size = 10, sort = "id", order = "asc", search) {
    return axios.get(
      `/api/v1/rooms?page=${page}&size=${size}&sort=${sort}&order=${order}&search=${search}`
    );
  }

  export function getRuanganById(id) {
    return axios.get(
      `/api/v1/rooms/{id}?id=${id}`
    );
  }

  export function saveRuangan(kode, namaRuangan, deskripsi, photo){
    return axios.post(`/api/v1/rooms`,{
        kode,
        namaRuangan,
        deskripsi, 
        photo
    })
}

export function updateRuangan(id, kode, namaRuangan, deskripsi, photo){
  return axios.patch(`/api/v1/rooms/${id}`,{
      kode,
      namaRuangan,
      deskripsi, 
      photo
  })
}

export function getKdo(page = 1, size = 10, sort = "id", order = "asc", search) {
    return axios.get(
      `/api/v1/kdos?page=${page}&size=${size}&sort=${sort}&order=${order}&search=${search}`
    );
  }
  export function getKdoById(id) {
    return axios.get(
      `/api/v1/kdos/{id}?id=${id}`
    );
  }

  export function saveKdo(jenis, nomorPlat, tahun, deskripsi, photo){
    return axios.post(`/api/v1/kdos`,{
        jenis,
        nomorPlat, 
        tahun,
        deskripsi, 
        photo
    })
}
export function updateKdo(id, jenis, nomorPlat, tahun, deskripsi, photo){
  return axios.patch(`/api/v1/kdos/${id}`,{
      jenis,
      nomorPlat,
      tahun,
      deskripsi, 
      photo
  })
}

  export function getBmn(page = 1, size = 10, sort = "id", order = "asc", search) {
    return axios.get(
      `/api/v1/bmns?page=${page}&size=${size}&sort=${sort}&order=${order}&search=${search}`
    );
  }
  export function getBmnById(id) {
    return axios.get(
      `/api/v1/bmns/{id}?id=${id}`
    );
  }
  export function saveBmn(kode, namaBmn, tahun, deskripsi, stock, photo){
    return axios.post(`/api/v1/bmns`,{
        kode,
        namaBmn, 
        tahun,
        deskripsi, 
        stock, 
        photo
    })
}
export function updateBmn(id, kode, namaBmn, tahun, deskripsi, stock, photo){
  return axios.patch(`/api/v1/bmns/${id}`,{
      kode,
      namaBmn,
      tahun,
      deskripsi,
      stock, 
      photo
  })
}

  export function getAtk() {
    return axios.get(
      `/api/v1/atk/listallatk`
    );
  }

  export function getAtkById(id) {
    return axios.get(
      `/api/v1/atk/atkbyid?id=${id}`
    );
  }

  export function tambahAtk(kode, namaAtk, deskripsi, photo, id=0, stock=0, harga=''){
    return axios.post(`/api/v1/atk/tambahatk?autoGenerateCode=false`,{
      kode,
      namaAtk,
      deskripsi,
      photo,
      id,
      stock,
      harga

      
    })
  }

  export function updateAtk(id, kode, namaAtk, deskripsi, photo, stock=0, harga=''){
    return axios.patch(`/api/v1/atk/editatk?id=${id}`,{
      kode,
      namaAtk,
      deskripsi,
      photo,
      stock, 
      harga
    })
  }

  export function opnameAtk(id, jumlah){
    return axios.patch(`/api/v1/atk/${id}/opname?jumlah=${jumlah}`)
  }

  export function checkAtkEmpty(){
    return axios.get(`/api/v1/atk/pembelian/draft`)
  }

  export function generateCode(){
    return axios.get(`/api/v1/atk/generatecode`)
  }

  export function createPurchase(nomorPo, tanggalPembelian){
    return axios.post(`/api/v1/atk/pembelian?tanggalPembelian=${tanggalPembelian}&nomorPo=${nomorPo}`)
  }


  export function saveDetailPurchaseAtk(atkPurchaseId, atkId, jumlah, harga, subtotal){
    return axios.post(`/api/v1/atk/pembelian/detail`,
    {
      atkPurchaseId,
      atkId,
      jumlah,
      harga,
      subTotal: subtotal
    })
  }

  export function deleteDetailPurchaseAtk(idAtk){
    return axios.delete(`/api/v1/atk/pembelian/detail/{id}?id=${idAtk}`)
  }

  export function cancelPurchaseAtk(idAtkPurchase){
    return axios.patch(`/api/v1/atk/pembelian/${idAtkPurchase}/batalkan`)
  }

  export function savePurchaseAtk(idAtkPurchase){
    return axios.patch(`/api/v1/atk/pembelian/selesai/${idAtkPurchase}`)
  }

  export function searchPurchase(nomorPo){
    return axios.get(`/api/v1/atk/pembelian/by_no_po/${nomorPo}`)
  }


  export function getSettings(){
    return axios.get(`/api/v1/settings`)
  }

  export function saveSlide(fileSlide, jenisSlide){
    return axios.patch(`/api/v1/settings/slide`,{
      fileSlide,
      jenisSlide
    })
  }
  export function saveText(text, jenisText){
    return axios.patch(`/api/v1/settings/text`,{
      text,
      jenisText
    })
  }
// gudang
  export function getGudang(){
    return axios.get(`/api/v1/gudangs`)
  }

  export function saveGudang(nama, code){
    return axios.post(`/api/v1/gudangs`,{
      nama,
      code
    })
  }

  export function updateGudang(id,nama, code){
    return axios.patch(`/api/v1/gudangs/${id}`,{
      nama,
      code
    })
  }

  export function deleteGudang(id){
    return axios.delete(`/api/v1/gudangs/${id}`)
  }

  export function getGudangById(id){
    return axios.get(`/api/v1/gudangs/${id}`)
  }

  // lemari
  export function getLemari(){
    return axios.get(`/api/v1/gudangs`)
  }

  export function getLemariById(id){
    return axios.get(`/api/v1/lemaris/${id}`)
  }

  export function getLemariByidGudang(id){
    return axios.get(`/api/v1/lemaris/gudang/${id}`)
  }

  export function deleteLemari(id){
    return axios.delete(`/api/v1/lemaris/${id}`)
  }

  export function saveLemari(idGudang, nama, code){
    return axios.post(`/api/v1/lemaris`,{
      nama,
      code,
      idGudang
    })
  }

  export function updateLemari(id,idGudang, nama, code){
    return axios.patch(`/api/v1/lemaris/${id}`,{
      nama,
      code,
      idGudang
    })
  }

  // rak
  export function getRakById(id){
    return axios.get(`/api/v1/raks/${id}`)
  }

  export function getRakByIdLemari(id){
    return axios.get(`/api/v1/raks/lemari/${id}`)
  }
  export function deleteRak(id){
    return axios.delete(`/api/v1/raks/${id}`)
  }

  export function saveRak(idLemari, nama, code){
    return axios.post(`/api/v1/raks`,{
      nama,
      code,
      idLemari
    })
  }

  export function updateRak(id,idLemari, nama, code){
    return axios.patch(`/api/v1/raks/${id}`,{
      nama,
      code,
      idLemari
    })
  }

  // boks
  export function getBoksById(id){
    return axios.get(`/api/v1/boxs/${id}`)
  }

  export function getBoksByIdRak(id){
    return axios.get(`/api/v1/boxs/rak/${id}`)
  }
  export function deleteBoks(id){
    return axios.delete(`/api/v1/boxs/${id}`)
  }

  export function saveBoks(idRak, nama, code){
    return axios.post(`/api/v1/boxs`,{
      nama,
      code,
      idRak
    })
  }

  export function updateBoks(id,idRak, nama, code){
    return axios.patch(`/api/v1/boxs/${id}`,{
      nama,
      code,
      idRak
    })
  }