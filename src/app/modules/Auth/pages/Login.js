import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import * as auth from "../_redux/authRedux";
import { getUserByToken, login } from "../_redux/authCrud";
import Card from "react-bootstrap/esm/Card";
import {toAbsoluteUrl} from "../../../../_metronic/_helpers"
/*
  INTL (i18n) docs:
  https://github.com/formatjs/react-intl/blob/master/docs/Components.md#formattedmessage
*/

/*
  Formik+YUP:
  https://jaredpalmer.com/formik/docs/tutorial#getfieldprops
*/

const initialValues = {
  email: "mhd.danil@gmail.com",
  password: "910222719",
};

function Login(props) {
  const { intl } = props;
  const [loading, setLoading] = useState(false);
  const LoginSchema = Yup.object().shape({
    email: Yup.string()
      .email("Harap masukkan format email yang sesuai")
      .min(3, "Minimal 3 karakter")
      .max(50, "Maksimal 50 karakter")
      .required("Email wajib diisi"),
    password: Yup.string()
    .min(3, "Minimal 3 karakter")
    .max(50, "Maksimal 50 karakter")
    .required("Password wajib diisi"),
  });

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const getInputClasses = (fieldname) => {
    if (formik.touched[fieldname] && formik.errors[fieldname]) {
      return "is-invalid";
    }

    if (formik.touched[fieldname] && !formik.errors[fieldname]) {
      return "is-valid";
    }

    return "";
  };

  const formik = useFormik({
    initialValues,
    validationSchema: LoginSchema,
    onSubmit: (values, { setStatus, setSubmitting }) => {
      enableLoading();
      setTimeout(() => {
        login(values.email, values.password)
          .then(({ data}) => {
            disableLoading();
            props.login(data.accessToken);
            getUserByToken(data.accessToken).then(({ data }) => {
              props.fulfillUser(data)
            })
          })
          .catch(() => {
            disableLoading();
            setSubmitting(false);
            setStatus(
              intl.formatMessage({
                id: "AUTH.VALIDATION.INVALID_LOGIN",
              })
            );
          });
      }, 1000);
    },
  });

  return (
    <div className="login-form login-signin" id="kt_login_signin_form">
    {/* begin::Head */}
    <Card style={{borderRadius:'2.5rem', backgroundColor:'transparent', border:'0'}}>
    <div className="text-center mb-1" style={{marginTop:"2.4rem"}}>
      <img
          src={toAbsoluteUrl("/media/logos/logo_asap.png")}
          alt="ASAP logo"
          style={{width:'50%'}}
      />

    </div>
      <div className="text-center text-dark mb-2">
        <h3 className="font-size-h1">
          {/* Login */}
          ASAP - Kanwil DJP Riau
        </h3>
        <p className="text-muted font-weight-bold">
          {/* ASAP - Kanwil DJP Riau */}
          Aplikasi ini digunakan oleh Direktorat Jenderal Pajak Kanwil DJP Riau
          </p>

      </div>

      <Card.Body>

    {/* end::Head */}

    {/*begin::Form*/}
    <form
      onSubmit={formik.handleSubmit}
      className="form fv-plugins-bootstrap fv-plugins-framework"
    >
      {formik.status ? (
        <div className="mb-10 alert alert-custom alert-light-danger alert-dismissible">
          <div className="alert-text font-weight-bold">{formik.status}</div>
        </div>
      ) : (
        <div >
        </div>
      )}

      <div className="form-group fv-plugins-icon-container">
        <input
          placeholder="Email"
          type="text"
          className={`form-control form-control-solid h-auto py-5 px-6 ${getInputClasses(
            "email"
          )}`}
          name="email"
          {...formik.getFieldProps("email")}
        />
        {formik.touched.email && formik.errors.email ? (
          <div className="fv-plugins-message-container">
            <div className="fv-help-block">{formik.errors.email}</div>
          </div>
        ) : null}
      </div>
      <div className="form-group fv-plugins-icon-container">
        <input
          placeholder="Password"
          type="password"
          autoComplete="on"
          className={`form-control form-control-solid h-auto py-5 px-6 ${getInputClasses(
            "password"
          )}`}
          name="password"
          {...formik.getFieldProps("password")}
        />
        {formik.touched.password && formik.errors.password ? (
          <div className="fv-plugins-message-container">
            <div className="fv-help-block">{formik.errors.password}</div>
          </div>
        ) : null}
      </div>
      <div className="text-center">

        <button
          id="kt_login_signin_submit"
          type="submit"
          disabled={formik.isSubmitting}
          className={`btn btn-primary font-weight-bold px-9 py-4 my-3`}
          style={{backgroundColor: "#263787", borderColor: "#263787", borderRadius:"2rem"}}
        >
          <span>Login</span>
          {loading && <span className="ml-3 spinner spinner-white"></span>}
        </button>
      </div>
    </form>
    <div className="mt-10 text-center">
					<span className="opacity-70 mr-4">
						Untuk membuat akun, silahkan hubungi bagian TURT - Kanwil DJP Riau
					</span>
					{/* <a href="javascript:;" id="kt_login_signup" className="text-muted text-hover-primary font-weight-bold">Sign Up!</a> */}
				</div>
      </Card.Body>

    </Card>
      <div className="text-dark-50 order-2 order-sm-1 my-2" style={{textAlign: 'center' }}>
          Copyright &copy; 2023 DJP Kanwil Riau
      </div>
  </div>

  );
}

export default injectIntl(connect(null, auth.actions)(Login));



// import React, { useState } from "react";
// import { Link } from "react-router-dom";
// import { useFormik } from "formik";
// import * as Yup from "yup";
// import { connect } from "react-redux";
// import { FormattedMessage, injectIntl } from "react-intl";
// import * as auth from "../_redux/authRedux";
// import { login } from "../_redux/authCrud";

// /*
//   INTL (i18n) docs:
//   https://github.com/formatjs/react-intl/blob/master/docs/Components.md#formattedmessage
// */

// /*
//   Formik+YUP:
//   https://jaredpalmer.com/formik/docs/tutorial#getfieldprops
// */

// const initialValues = {
//   email: "admin@demo.com",
//   password: "demo",
// };

// function Login(props) {
//   const { intl } = props;
//   const [loading, setLoading] = useState(false);
//   const LoginSchema = Yup.object().shape({
//     email: Yup.string()
//       .email("Wrong email format")
//       .min(3, "Minimum 3 symbols")
//       .max(50, "Maximum 50 symbols")
//       .required(
//         intl.formatMessage({
//           id: "AUTH.VALIDATION.REQUIRED_FIELD",
//         })
//       ),
//     password: Yup.string()
//       .min(3, "Minimum 3 symbols")
//       .max(50, "Maximum 50 symbols")
//       .required(
//         intl.formatMessage({
//           id: "AUTH.VALIDATION.REQUIRED_FIELD",
//         })
//       ),
//   });

//   const enableLoading = () => {
//     setLoading(true);
//   };

//   const disableLoading = () => {
//     setLoading(false);
//   };

//   const getInputClasses = (fieldname) => {
//     if (formik.touched[fieldname] && formik.errors[fieldname]) {
//       return "is-invalid";
//     }

//     if (formik.touched[fieldname] && !formik.errors[fieldname]) {
//       return "is-valid";
//     }

//     return "";
//   };

//   const formik = useFormik({
//     initialValues,
//     validationSchema: LoginSchema,
//     onSubmit: (values, { setStatus, setSubmitting }) => {
//       enableLoading();
//       setTimeout(() => {
//         login(values.email, values.password)
//           .then(({ data: { accessToken } }) => {
//             disableLoading();
//             props.login(accessToken);
//           })
//           .catch(() => {
//             disableLoading();
//             setSubmitting(false);
//             setStatus(
//               intl.formatMessage({
//                 id: "AUTH.VALIDATION.INVALID_LOGIN",
//               })
//             );
//           });
//       }, 1000);
//     },
//   });

//   return (
//     <div className="login-form login-signin" id="kt_login_signin_form">
//       {/* begin::Head */}
//       <div className="text-center mb-10 mb-lg-20">
//         <h3 className="font-size-h1">
//           Login
//         </h3>
//         <p className="text-muted font-weight-bold">
//           Masukkan Email dan Password.
//         </p>
//       </div>
//       {/* end::Head */}

//       {/*begin::Form*/}
//       <form
//         onSubmit={formik.handleSubmit}
//         className="form fv-plugins-bootstrap fv-plugins-framework"
//       >
//         {formik.status ? (
//           <div className="mb-10 alert alert-custom alert-light-danger alert-dismissible">
//             <div className="alert-text font-weight-bold">{formik.status}</div>
//           </div>
//         ) : null}

//         <div className="form-group fv-plugins-icon-container">
//           <input
//             placeholder="Email"
//             type="email"
//             className={`form-control form-control-solid h-auto py-5 px-6 ${getInputClasses(
//               "email"
//             )}`}
//             name="email"
//             {...formik.getFieldProps("email")}
//           />
//           {formik.touched.email && formik.errors.email ? (
//             <div className="fv-plugins-message-container">
//               <div className="fv-help-block">{formik.errors.email}</div>
//             </div>
//           ) : null}
//         </div>
//         <div className="form-group fv-plugins-icon-container">
//           <input
//             placeholder="Password"
//             type="password"
//             className={`form-control form-control-solid h-auto py-5 px-6 ${getInputClasses(
//               "password"
//             )}`}
//             name="password"
//             {...formik.getFieldProps("password")}
//           />
//           {formik.touched.password && formik.errors.password ? (
//             <div className="fv-plugins-message-container">
//               <div className="fv-help-block">{formik.errors.password}</div>
//             </div>
//           ) : null}
//         </div>
//         <div className="form-group d-flex flex-wrap justify-content-between align-items-center">
//           <button
//             id="kt_login_signin_submit"
//             type="submit"
//             disabled={formik.isSubmitting}
//             className={`btn btn-primary font-weight-bold px-9 py-4 my-3`}
//           >
//             <span>Login</span>
//             {loading && <span className="ml-3 spinner spinner-white"></span>}
//           </button>
//         </div>
//       </form>
//       {/*end::Form*/}
//     </div>
//   );
// }

// export default injectIntl(connect(null, auth.actions)(Login));
