/* eslint-disable react-hooks/exhaustive-deps */
/* Library */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";
import * as columnFormatters from "../../helpers/column-formatters";
// import { getRuangan } from "../../references/Api";

/* Utility */

function ArsipTable({ tab = "proses" }) {
  const history = useHistory();
  // const [content, setContent] = useState([]);
  const [data, setData] = useState([]);
  const [searchTxt, setSearchTxt] = useState("");
  const [currentPage, setCurrentPage] = useState(0);
  const [sizePage, setSizePage] = useState(10);
  const [loading, setLoading] = useState(true);

  const content = [
    {
      id: "1",
      kode: "808080919191",
      judul: "SPT Tahunan | Budi Eko Prasetyo",
      ready: 0,
      tipe: "SPT Tahunan",
      tahun: "2019"
      // photo: "/media/users/user.png",
    },
    {
      id: "2",
      kode: "8021810109181",
      judul: "Pemindahbukuan | Tukul Adi Sutopo",
      ready: 0,
      tipe: "Pemindahbukuan",
      tahun: "2022"
      // photo: "/media/users/user.png",
    },
    {
      id: "3",
      kode: "82918191910191",
      judul: "Surat Ketetapan Pajak | Rinna Nanda Kusuma",
      ready: 0,
      tipe: "Surat Ketetapan Pajak",
      tahun: "2020"
      // photo: "/media/users/user.png",
    },
    {
      id: "4",
      kode: "87929289001091",
      judul: "Tagihan Pajak | Eko Prasetyo",
      ready: 1,
      tipe: "Tagihan Pajak",
      tahun: "2021"
      // photo: "/media/users/user.png",
    },
  ];

  // const add = () => history.push("/master/ruangan/tambah");
  const edit = (id) =>
      history.push(
        `/arsip/${id}/edit`
      );

  const view = (id) => 
  history.push(
    `/arsip/${id}/view`
  );
 

 

  useEffect(() => {
  }, []);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      hidden: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage === 0 ?  currentPage + 1 - 1 : currentPage -1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      // style: {
      //   width: "100px",
      // },
    },
    {
      dataField: "id",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "kode",
      text: "Kode Arsip",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // style: {
      //   width: "10px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
  
    {
      dataField: "judul",
      text: "Judul Arsip",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // style: {
      //   width: "100px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "tipe",
      text: "Tipe Arsip",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // style: {
      //   width: "100px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "tahun",
      text: "Tahun Arsip",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // style: {
      //   width: "100px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "ready",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterDataArsip,
      // style: {
      //   width: "80px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    // {
    //   dataField: "status",
    //   text: "Status",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   hidden: tab === 'selesai' || tab === 'tolak' ? true : false,
    //   formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeUsulanMatrix,
    // },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterDataArsip,
      formatExtraData: {
        openEditDialog: edit,
        openDetailDialog: view
        // openDeleteDialog: deleteAction,
        // publishKnowledge: apply,
        // showReview: review,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      // style: {
      //   minWidth: "100px",
      // },
    },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id",
    pageNumber: currentPage === 0 ? currentPage + 1 : currentPage === 1 ? currentPage : currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "id", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: data.totalElements,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  // const emptyDataMessage = () => {
  //   return "Tidak Ada Data";
  // };
  const { SearchBar } = Search;
  const handleTableChange = (
    type,
    { sortField, sortOrder, data, filters, ...props }
  ) => {
   
  };
  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                            placeholder="Cari"
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        // remote
                        // onTableChange={handleTableChange}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination
                        paginationProps={paginationProps}
                          // isLoading={loading}
                      />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default ArsipTable;
