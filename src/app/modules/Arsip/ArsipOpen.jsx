import React from "react";
import SVG from "react-inlinesvg";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../_metronic/_partials/controls";
import { toAbsoluteUrl } from "../../../_metronic/_helpers";

// Import Worker
import { Worker } from "@react-pdf-viewer/core";
// Import the main component
import { Viewer, SpecialZoomLevel } from "@react-pdf-viewer/core";
// Import Default Layout Plugin
import { defaultLayoutPlugin } from "@react-pdf-viewer/default-layout";
// Import the styles
import "@react-pdf-viewer/core/lib/styles/index.css";
// Import the styles of default layout plugin
import "@react-pdf-viewer/default-layout/lib/styles/index.css";
function ArsipOpen({
  history,
  match: {
    params: { id },
  },
}) {
  const defaultLayoutPluginInstance = defaultLayoutPlugin();
  const backAction = () => {
    history.push("/arsip");
  };
  return (
    <>
      <div className="d-flex flex-row">
        <div
          className="flex-row-auto offcanvas-mobile w-350px w-xxl-450px"
          id="kt_profile_aside"
        >
          <div className="card card-custom card-stretch">
            <div className="card-body pt-4">
              <div className="py-5">
                <div className="d-flex align-items-center justify-content-between mb-9">
                  <h5 className="font-weight-bold">RINCIAN ARSIP</h5>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-9">
                  <span className="font-weight-normal mr-10">
                    Kode Pemberkasan:
                  </span>
                  <span className="text-muted" style={{ textAlign: "justify" }}>
                    808080919191
                  </span>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-9">
                  <span className="font-weight-normal mr-10">Judul:</span>
                  <span className="text-muted" style={{ textAlign: "justify" }}>
                    SPT Tahunan | Budi Eko Prasetyo
                  </span>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-9">
                  <span className="font-weight-normal mr-10">Tipe:</span>
                  <span className="text-muted" style={{ textAlign: "justify" }}>
                    SPT Tahunan
                  </span>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-9">
                  <span className="font-weight-normal mr-10">Tahun:</span>
                  <span className="text-muted" style={{ textAlign: "justify" }}>
                    2019
                  </span>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-9">
                  <span className="font-weight-normal mr-10">Deskripsi:</span>
                  <span className="text-muted" style={{ textAlign: "justify" }}>
                    SPT Tahunan dari Wajib Pajak Budi Eko Prasetyo yang telah
                    diarsipkan oleh salah satu petugas pajak di KPP Pratama
                    Lubuk Linggau.
                  </span>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-9">
                  <span className="font-weight-normal mr-10">
                    Jumlah Lembar:
                  </span>
                  <span className="text-muted" style={{ textAlign: "justify" }}>
                    10
                  </span>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-9">
                  <span className="font-weight-normal mr-10">
                    Lokasi Penyimpanan:
                  </span>
                  <span className="text-muted" style={{ textAlign: "justify" }}>
                    E-3-IV-2
                  </span>
                </div>
              </div>
              <div
            className="row mt-3"
            style={{ display: "block", textAlign: "right" }}
          >
             <button
              type="button"
              className="btn btn-light ml-2"
              onClick={backAction}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
          </div>
            </div>
          </div>
        </div>
        <div className="flex-row-fluid ml-lg-8">
          {/* <PembelianTambahTable /> */}
          <div className="card card-custom card-stretch">
            <div className="card-body">
              <div
                className="col-lg-12"
                style={{
                  border: "1px solid rgba(0, 0, 0, 0.3)",
                  height: "700px",
                }}
              >
                <Worker workerUrl={toAbsoluteUrl("/pdfworkermin.js")}>
                  <Viewer
                    fileUrl={toAbsoluteUrl("/BPMN_0401032238.pdf")}
                    // plugins={[defaultLayoutPluginInstance]}
                    defaultScale={SpecialZoomLevel.PageFit}
                  />
                </Worker>
              </div>
            
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ArsipOpen;
