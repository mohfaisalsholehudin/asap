import React,  { Suspense }  from "react";
import { useSelector } from "react-redux";
import {Redirect, Switch, Route } from "react-router-dom";
import {LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";
import Pengaturan from "./pengaturan/Pengaturan";
import TipeArsip from "./tipe-arsip/TipeArsip";
import TipeArsipEdit from "./tipe-arsip/TipeArsipEdit";
import Pemeliharaan from "./pemeliharaan/Pemeliharaan";
import PemeliharaanEdit from "./pemeliharaan/PemeliharaanEdit";
import Lokasi from "./lokasi/Lokasi";
import LokasiEdit from "./lokasi/LokasiEdit";
import Lemari from "./lokasi/lemari/Lemari";
import LemariEdit from "./lokasi/lemari/LemariEdit";
import Rak from "./lokasi/rak/Rak";
import RakEdit from "./lokasi/rak/RakEdit";
import Boks from "./lokasi/boks/Boks";
import BoksEdit from "./lokasi/boks/BoksEdit";


export default function Administrator() {

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
    <Switch>
          
        <Redirect
            exact={true}
            from="/admin"
            to="/admin/pengaturan"
        />


      {/* Begin Pengaturan */}

         {/* Begin Setting Knowledge - Alasan Tolak*/}
         {/* <ContentRoute path="/admin/pengaturan/tolak/:id/edit" component={MasterTolakEdit} /> */}
         {/* <ContentRoute path="/admin/pengaturan/tolak/add" component={MasterTolakEdit} /> */}
         <ContentRoute path="/admin/pengaturan" component={Pengaturan} />
         {/* End of Setting Knowledge - Alasan Tolak*/}



      {/* End of Pengaturan */}

      {/* Begin Tipe Arsip */}
      <ContentRoute path="/admin/tipe-arsip/tambah" component={TipeArsipEdit} />
      <ContentRoute path="/admin/tipe-arsip" component={TipeArsip} />

      {/* End of Tipe Arsip */}


      {/* Begin Lokasi Penyimpanan */}
      <ContentRoute path="/admin/lokasi/:id/lemari/:id_lemari/rak/:id_rak/boks/:id_boks/edit" component={BoksEdit} />
      <ContentRoute path="/admin/lokasi/:id/lemari/:id_lemari/rak/:id_rak/boks/add" component={BoksEdit} />
      <ContentRoute path="/admin/lokasi/:id/lemari/:id_lemari/rak/:id_rak/boks" component={Boks} />
      <ContentRoute path="/admin/lokasi/:id/lemari/:id_lemari/rak/:id_rak/edit" component={RakEdit} />
      <ContentRoute path="/admin/lokasi/:id/lemari/:id_lemari/rak/add" component={RakEdit} />
      <ContentRoute path="/admin/lokasi/:id/lemari/:id_lemari/rak" component={Rak} />
      <ContentRoute path="/admin/lokasi/:id/lemari/:id_lemari/edit" component={LemariEdit} />
      <ContentRoute path="/admin/lokasi/:id/lemari/add" component={LemariEdit} />
      <ContentRoute path="/admin/lokasi/:id/lemari" component={Lemari} />
      <ContentRoute path="/admin/lokasi/:id/edit" component={LokasiEdit} />
      <ContentRoute path="/admin/lokasi/tambah" component={LokasiEdit} />
      <ContentRoute path="/admin/lokasi" component={Lokasi} />
      {/* End of Lokasi Penyimpanan */}

       {/* Begin Jenis Pemeliharaan */}
       <ContentRoute path="/admin/pemeliharaan-jenis/tambah" component={PemeliharaanEdit} />
       <ContentRoute path="/admin/pemeliharaan-jenis" component={Pemeliharaan} />
       {/* End of Jenis Pemeliharaan */}





    </Switch>
  </Suspense>

  );
}
