import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Input, Select as Sel } from "../../../../helpers";

function BoksEditForm({ content, btnRef, saveForm }) {
  const [show, setShow] = useState(false);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nama: Yup.string()
      .required("Nama Boks wajib diisi"),
    code: Yup.string()
      .required("Kode Boks wajib diisi"),
  });


  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
          // console.log(values)
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid,
        }) => {
          // const handleCheck = (e) => {
          //   setFieldValue("seluruh_pegawai", e.target.checked);
          //   setCheck(e.target.checked);
          // };
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Nama */}
                <div className="form-group row">
                  <Field
                    name="nama"
                    component={Input}
                    placeholder="Nama Boks"
                    label="Nama Boks"
                  />
                </div>
                 {/* Field Kode */}
                 <div className="form-group row">
                  <Field
                    name="code"
                    component={Input}
                    placeholder="Kode Boks"
                    label="Kode Boks"
                  />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default BoksEditForm;
