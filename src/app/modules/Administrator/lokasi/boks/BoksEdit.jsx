/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { getBoksById, getLemariById, getRakById, saveBoks, saveLemari, saveRak, updateBoks, updateLemari, updateRak } from "../../../references/Api";
import BoksEditForm from "./BoksEditForm";

function BoksEdit({
  history,
  match: {
    params: { id, id_lemari, id_rak, id_boks }
  }
}) {
  const initValues = {
    nama: "",
    code: "",
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();

  useEffect(() => {
    let _title = id_boks
      ? "Edit Boks"
      : "Tambah Boks";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id_boks) {
      getBoksById(id_boks).then(({data})=> {
        setContent(data)
      })
    }
  }, [id_boks, suhbeader]);
  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push(`/admin/lokasi/${id}/lemari/${id_lemari}/rak/${id_rak}/boks`);
  };
  const saveForm = values => {
    // console.log(values)
    if (!id_boks) {
        saveBoks(id_rak, values.nama, values.code).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/admin/lokasi/${id}/lemari/${id_lemari}/rak/${id_rak}/boks`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/admin/lokasi/${id}/lemari/${id_lemari}/rak/${id_rak}/boks/add`);
              });
            }
          }
        );
    } else {
        updateBoks( id_boks, id_rak, values.nama, values.code).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/admin/lokasi/${id}/lemari/${id_lemari}/rak/${id_rak}/boks`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/admin/lokasi/${id}/lemari/${id_lemari}/rak/${id_rak}/boks/add`);
              });
            }
          }
        );
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <BoksEditForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default BoksEdit;
