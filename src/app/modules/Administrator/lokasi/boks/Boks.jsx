/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { getGudangById, getLemariById, getRakById } from "../../../references/Api";
import BoksTable from "./BoksTable";

function Boks({
  history,
  match: {
    params: { id, id_lemari, id_rak }
  }
}) {
  const [detil, setDetil] = useState([]);
  const [lemari, setLemari] = useState([]);
  const [rak, setRak] = useState([]);

  useEffect(() => {
    getGudangById(id).then(({ data }) => {
      setDetil(data);
      getLemariById(id_lemari).then(({data})=> {
        setLemari(data)
        getRakById(id_rak).then(({data})=> {
          setRak(data)
        })
      })
    });
  }, [id]);

  const handleBack = () => {
    history.push(`/admin/lokasi/${id}/lemari/${id_lemari}/rak`)
  }
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Boks"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <div className="row">
              <div className="col-xl-12 col-lg-12 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Nama Gudang
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${detil.nama}`}
                      {/* : Business Process 1 */}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-12 col-lg-12 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Kode Gudang
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {/* {`: ${DateFormat(detil.tgl_penyusunan)}`} */}
                      {`: ${detil.code}`}
                      {/* : Keterangan Business Process 1 */}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-12 col-lg-12 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Nama Lemari
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${lemari.nama}`}
                      {/* : Business Process 1 */}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-12 col-lg-12 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Kode Lemari
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {/* {`: ${DateFormat(lemari.tgl_penyusunan)}`} */}
                      {`: ${lemari.code}`}
                      {/* : Keterangan Business Process 1 */}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-12 col-lg-12 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Nama Rak
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${rak.nama}`}
                      {/* : Business Process 1 */}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-12 col-lg-12 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Kode Rak
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {/* {`: ${DateFormat(lemari.tgl_penyusunan)}`} */}
                      {`: ${rak.code}`}
                      {/* : Keterangan Business Process 1 */}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </>
          <BoksTable id={id} id_lemari={id_lemari} id_rak={id_rak} />
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
          <>
          <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={handleBack}
            className="btn btn-light-success"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          
        </div>
          </>
      </CardFooter>
      </Card>
    </>
  );
}

export default Boks;
