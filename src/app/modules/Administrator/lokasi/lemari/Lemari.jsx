import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { getGudangById } from "../../../references/Api";
import LemariTable from "./LemariTable";

function Lemari({
  history,
  match: {
    params: { id }
  }
}) {
  const [detil, setDetil] = useState([]);

  useEffect(() => {
    getGudangById(id).then(({ data }) => {
      setDetil(data);
    });
  }, [id]);

  const handleBack = () => {
    history.push(`/admin/lokasi`)
  }
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Lemari"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <div className="row">
              <div className="col-xl-12 col-lg-12 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Nama Gudang
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${detil.nama}`}
                      {/* : Business Process 1 */}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-12 col-lg-12 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Kode Gudang
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {/* {`: ${DateFormat(detil.tgl_penyusunan)}`} */}
                      {`: ${detil.code}`}
                      {/* : Keterangan Business Process 1 */}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </>
          <LemariTable id={id} />
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
          <>
          <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={handleBack}
            className="btn btn-light-success"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          
        </div>
          </>
      </CardFooter>
      </Card>
    </>
  );
}

export default Lemari;
