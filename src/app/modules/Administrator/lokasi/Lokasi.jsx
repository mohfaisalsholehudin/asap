import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import LokasiTable from "./LokasiTable";

function Lokasi() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Lokasi Penyimpanan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <LokasiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Lokasi;
