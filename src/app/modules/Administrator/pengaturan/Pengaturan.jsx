/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import SVG from "react-inlinesvg";
import { Field, Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../_metronic/_partials/controls";
import Slide1 from "./Slide1";
import Slide2 from "./Slide2";
import Slide3 from "./Slide3";
import Slide4 from "./Slide4";
import Slide5 from "./Slide5";
import Slide6 from "./Slide6";
import Kasubbag from "./Kasubbag";
import { getSettings } from "../../references/Api";

function Pengaturan() {
  const history = useHistory();
  const [content, setContent] = useState()

  const backAction = () => {
    history.push("/master/pengguna");
  };

  useState(()=> {
    getSettings().then(({data})=> {
      setContent({
        text1: data.text1,
        text2: data.text2,
        text3: data.text3,
        text4: data.text4,
        text5: data.text5,
        text6: data.text6,
        slide1: data.slide1,
        slide2: data.slide2,
        slide3: data.slide3,
        slide4: data.slide4,
        slide5: data.slide5,
        slide6: data.slide6,
        kasubagId: data.kasubagId
      })
    })
  },[])
  return (
    <>
      <Card>
        <CardHeader
          title="Pengaturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <div className="row">
            <div className="col-lg-6 col-xl-6">
              <Slide1
              content={content}
              />
            </div>
            <div className="col-lg-6 col-xl-6">
              <Slide2 
              content={content}
              />
            </div>
          </div>
          <div className="separator separator-dashed my-5"></div>
          <div className="row">
            <div className="col-lg-6 col-xl-6">
              <Slide3 
              content={content}
              />
            </div>
            <div className="col-lg-6 col-xl-6">
              <Slide4 
              content={content}
              />
            </div>
          </div>
          <div className="separator separator-dashed my-5"></div>
          <div className="row">
            <div className="col-lg-6 col-xl-6">
              <Slide5 
              content={content}
              />
            </div>
            <div className="col-lg-6 col-xl-6">
              <Slide6 
              content={content}
              />
            </div>
          </div>
          {/* <div className="separator separator-dashed my-5"></div>
          <Kasubbag /> */}
        </CardBody>
        {/* <CardFooter style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "left" }}>
          <button
            type="submit"
            className="btn btn-success ml-2"
            // onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter> */}
      </Card>
    </>
  );
}

//#a6c8e6
export default Pengaturan;
