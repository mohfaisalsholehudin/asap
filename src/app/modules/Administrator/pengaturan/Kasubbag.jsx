import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { SelectKasubbag as Sel } from "../../../helpers";

function Kasubbag() {
  const initialValues = {
    es4: "",
  };
  const ProposalEditSchema = Yup.object().shape({
    es4: Yup.string().required("Kasubbag wajib diisi"),
  });
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          // saveForm(values);
        }}
      >
        {({ handleSubmit }) => {
          return (
            <>
              <div className="row">
                {/* <label className="col-xl-3"></label> */}
                <div className="col-lg-12 col-xl-12">
                  <h5 className="font-weight-bold mb-6">Ganti Kasubbag</h5>
                </div>
              </div>
              <Form className="form form-label-right">
                <div className="form-group row">
                  {/* <div className="col-xl-3 col-lg-3"></div> */}
                  <div
                    className="col-lg-9 col-xl-9"
                    style={{
                      paddingTop: "10px",
                      color: "white",
                      backgroundColor: "#1aa1ff",
                      borderRadius: "1rem",
                      marginLeft: "17px",
                    }}
                  >
                    <p>Kasubbag saat ini adalah: Deki Setiawan.</p>
                    <p>
                      {" "}
                      Kasubbag sebagai penandatangan laporan. Merubah Kasubbag
                      berarti merubah Hak Akses Kasubbag pada aplikasi android.{" "}
                    </p>
                  </div>
                </div>
                {/* Field Role */}
                <div className="form-group row">
                  <Sel name="es4" label="Nama Kasubbag">
                    <option value="3">Deki Setiawan</option>
                    <option value="2">Burhanuddin</option>
                  </Sel>
                </div>
                <button
                  type="submit"
                  className="btn btn-success ml-2"
                  // onClick={saveButton}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="fas fa-save"></i>
                  Simpan Kasubbag
                </button>
                {/* <button
                  type="submit"
                  style={{ display: "none" }}
                  // ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button> */}
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default Kasubbag;
