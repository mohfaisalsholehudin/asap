/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import { Field, Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import CustomFileInput from "../../../helpers/form/CustomFileInput";
import { saveSlide, saveText, uploadFile } from "../../references/Api";
import { typeOf } from "react-is";

const { FILE_URL } = window.ENV;

function Slide2({ content }) {
  const history = useHistory();
  const [preview, setPreview] = useState(null);
  const [isDisabled, setIsDisabled] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    // setPic("/media/stock-600x400/img-14.jpg");
  }, []);

  const initialValues = {
    slide2: "",
    text2: "",
  };
  const validationSchema = Yup.object().shape({
    slide2: Yup.mixed()
      // .required("A file is required")
      .test(
        "fileSize",
        "Ukuran file terlalu besar.",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Format file tidak sesuai.",
        (value) =>
          value && SUPPORTED_FORMATS.some((a) => value.type.includes(a))
      ),
  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = [
    // jpg
    "image/jpeg",
  ];
  const enableLoading = () => {
    setLoading(true);
    setIsDisabled(true);
  };

  const disableLoading = () => {
    setLoading(false);
    setIsDisabled(false);
  };

  const saveForm = (val) => {
    if (val.slide2.name) {
      enableLoading(true);
      const formData = new FormData();
      formData.append("file", val.slide2);
      uploadFile(formData).then(({ data }) => {
        disableLoading(false);
        saveSlide(data, 2).then(({ status }) => {
          if (status === 200) {
            saveText(val.text2, 2).then(({ status }) => {
              if (status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push("/dashboard");
                    history.replace("/admin/pengaturan");
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.replace("/admin/pengaturan");
                });
              }
            });
          }
        });
      });
    } else {
      if (val.slide2.file) {
        saveSlide(val.slide2.file, 2).then(({ status }) => {
          if (status === 200) {
            saveText(val.text2, 2).then(({ status }) => {
              if (status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push("/dashboard");
                    history.replace("/admin/pengaturan");
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.replace("/admin/pengaturan");
                });
              }
            });
          }
        });
      } else {
        swal({
          title: "Unggah file terlebih dahulu!",
          icon: "info",
          closeOnClickOutside: false,
        }).then((willApply) => {
          if (willApply) {
            // history.push("/logout");
          }
        });
      }
    }
  };
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content || initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          saveForm(values);
          // console.log(values);
        }}
      >
        {({
          handleSubmit,
          values,
          errors,
          setFieldValue,
          setErrors,
          getFieldProps,
        }) => {
          const handleChangeSlide = (e) => {
            setFieldValue("slide2", e.target.files[0]);
          };

          const getSlidePreview = () => {
            if (values.slide2.name) {
              const reader = new FileReader();
              reader.readAsDataURL(values.slide2);
              reader.onload = () => {
                setPreview(reader.result);
              };
            } else if (typeof values.slide2 === "string") {
              setPreview(toAbsoluteUrl(FILE_URL + values.slide2));
              setFieldValue("slide2", {
                file: values.slide2,
                size: 5000000,
                type: ["image/jpeg"],
              });
            }
          };

          const removeSlide = () => {
            setPreview("");
            setErrors("");
            setFieldValue("slide2", {
              file: null,
              size: 5000000,
              type: ["image/jpeg"],
            });
          };
          return (
            <>
              <div className="row">
                {/* <label className="col-xl-3"></label> */}
                <div className="col-lg-12 col-xl-12">
                  <h5 className="font-weight-bold mb-6">Slide 2</h5>
                </div>
              </div>
              <Form className="form form-label-right">
                <div className="form-group row">
                  {/* <label className="col-xl-3 col-lg-3 col-form-label">
                        
                      </label> */}
                  <div className="col-lg-12 col-xl-12">
                    <div
                      className="image-input image-input-outline"
                      id="kt_profile_avatar"
                      style={{
                        backgroundImage: `url(${toAbsoluteUrl(
                          "/media/logos/slide-null.jpeg"
                        )}`,
                        width: "300px",
                        height: "200px",
                        backgroundSize: "contain",
                      }}
                    >
                      {/* <div
                            className="image-input-wrapper"
                            style={{
                              // backgroundImage: `${getSlidePreview()}`,
                              // backgroundImage: `${values.slide1 && getSlidePreview()}`,
                              width: "300px",
                              height: "200px",
                              // border: `${errors.slide1 && "3px solid red"}`
                              border: `${errors.slide1 && "3px solid red"}`,
                            }}
                          /> */}
                      {preview ? (
                        <div>
                          <img
                            style={{
                              width: "300px",
                              height: "200px",
                              border: `${errors.slide2 ? "3px solid red" : ""}`,
                            }}
                            src={preview}
                            alt=""
                          />
                        </div>
                      ) : (
                        <div
                          className="image-input-wrapper"
                          style={{
                            // backgroundImage: `${getSlidePreview()}`,
                            // backgroundImage: `${values.slide1 && getSlidePreview()}`,
                            width: "300px",
                            height: "200px",
                            border: `${errors.slide2 && "3px solid red"}`,
                          }}
                        />
                      )}
                      {values.slide2 && getSlidePreview()}
                      {/* {content} */}
                      <label
                        className="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                        data-action="change"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Change avatar"
                      >
                        <i className="fa fa-pen icon-sm text-muted"></i>
                        <input
                          type="file"
                          name="slide2"
                          accept=".jpg, .jpeg"
                          onChange={(e) => handleChangeSlide(e)}
                        />
                        <input type="hidden" name="profile_avatar_remove" />
                      </label>
                      <span
                        className="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                        data-action="cancel"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Cancel avatar"
                      >
                        <i className="ki ki-bold-close icon-xs text-muted"></i>
                      </span>
                      <span
                        onClick={removeSlide}
                        className="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                        data-action="remove"
                        data-toggle="tooltip"
                        title=""
                        data-original-title="Remove avatar"
                      >
                        <i className="ki ki-bold-close icon-xs text-muted"></i>
                      </span>
                    </div>
                    <span className="form-text text-muted">
                      Tipe file yang diperbolehkan: jpg, jpeg.
                    </span>
                    <span className="form-text text-muted">Maks. 5 MB.</span>
                    {errors.slide2 && (
                      <span className="form-text" style={{ color: "red" }}>
                        {errors.slide2}
                      </span>
                    )}
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-lg-10 col-xl-10">
                    <textarea
                      type="text"
                      placeholder="Teks Slide 2"
                      className={`form-control form-control-lg form-control-solid`}
                      name="text2"
                      {...getFieldProps("text2")}
                    />
                  </div>
                </div>
                {loading ? (
                  <button
                    type="submit"
                    className="btn btn-success spinner spinner-white spinner-left ml-2"
                    onSubmit={handleSubmit}
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                    disabled={isDisabled}
                  >
                    <span>Menyimpan</span>
                  </button>
                ) : (
                  <button
                    type="submit"
                    className="btn btn-success ml-2"
                    onSubmit={handleSubmit}
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                    // disabled={disabled}
                  >
                    <i className="fas fa-save"></i>
                    Simpan Slide 2
                  </button>
                )}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

//#a6c8e6
export default Slide2;
