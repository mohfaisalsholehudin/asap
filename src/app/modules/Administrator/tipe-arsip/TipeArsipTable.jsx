/* Library */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */

function TipeArsipTable() {
  const history = useHistory(); 
  // const [content, setContent] = useState([]);
  // const [loading, setLoading] = useState(true);


  const add = () => history.push("/admin/tipe-arsip/tambah");
  const edit = id => history.push(`/admin/tipe-arsip/${id}/edit`);

  const content =[
    {
      id: "1",
      nama: "SPT Tahunan"
    },
    {
      id: "2",
      nama: "SPT Masa"
    },
    {
      id: "3",
      nama: "Pengukuhan PKP"
    },
    {
      id: "4",
      nama: "Pemindahbukuan"
    },
    {
      id: "5",
      nama: "Tagihan Pajak"
    },
    {
      id: "6",
      nama: "SPMKP"
    },
    {
      id: "7",
      nama: "Surat Ketetapan Pajak"
    },
  ]


    useEffect(()=> {
      // getTipeKnowledge().then(({ data }) => {
      //   setLoading(false)
      //   setContent(data)
      // })
    },[])


  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nama",
      text: "Nama",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    // {
    //   dataField: "jenis",
    //   text: "Jenis",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses
    // },
    // {
    //   dataField: "template",
    //   text: "Template",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses
    // },
    // {
    //   dataField: "updated",
    //   text: "Waktu Edit",
    //   sort: true,
    //   formatter: columnFormatters.DateFormatterProbis,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    // },
    // {
    //   dataField: "seluruh_pegawai",
    //   text: "Seluruh Pegawai",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses
    // },
    // {
    //   dataField: "status",
    //   text: "Status",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   // formatter: columnFormatters.StatusColumnFormatterAdminSettingProbis,
    // },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterAdminTipeArsip,
      formatExtraData: {
        openEditDialog: edit,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "status",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "status", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  // const emptyDataMessage = () => {
  //   return "Tidak Ada Data";
  // };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_tipe_km"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                            placeholder='Cari'
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right"
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination 
                      paginationProps={paginationProps} 
                      // isLoading={loading} 
                      />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default TipeArsipTable;
