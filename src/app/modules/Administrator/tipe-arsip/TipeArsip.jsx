import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import TipeArsipTable from "./TipeArsipTable";

function TipeArsip() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Tipe Arsip"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <TipeArsipTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default TipeArsip;
