import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Input, Select as Sel } from "../../../helpers";

function TipeArsipEditForm({ content, btnRef, saveForm }) {
  const [show, setShow] = useState(false);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nama: Yup.string()
      .required("Nama wajib diisi"),
    // seluruh_pegawai: Yup.string().required("Ceklis is required"),
  });

  useEffect(() => {
    // getMasterJenis().then(({ data }) => {
    //   data.map(data => {
    //     return setMaster(master => [
    //       ...master,
    //       {
    //         label: data.nama,
    //         value: data.id
    //       }
    //     ]);
    //   });
    // });
  }, []);
  // useEffect(() => {
  //   content.status !== "" ? setShow(true) : setShow(false);
  //   content.seluruh_pegawai === "true" ? setCheck(true) : setCheck(false);
  // }, [content]);

  // const ceklis = [
  //   { nama: "Ya", value: "Ya" },
  //   { nama: "Tidak", value: "Tidak" },
  // ];
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
          // console.log(values)
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid,
        }) => {
          // const handleCheck = (e) => {
          //   setFieldValue("seluruh_pegawai", e.target.checked);
          //   setCheck(e.target.checked);
          // };
          return (
            <>
              <Form className="form form-label-right">
                {/* Field nama */}
                <div className="form-group row">
                  <Field
                    name="nama"
                    component={Input}
                    placeholder="Nama"
                    label="Nama"
                  />
                </div>
                {/* FIELD STATUS */}
                {show ? (
                  <div className="form-group row">
                    <Sel name="status" label="Status">
                      <option>Pilih Status</option>
                      <option value="1">AKTIF</option>
                      <option value="0">TIDAK AKTIF</option>
                    </Sel>
                  </div>
                ) : null}

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default TipeArsipEditForm;
