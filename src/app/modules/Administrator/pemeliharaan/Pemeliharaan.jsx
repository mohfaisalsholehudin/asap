import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import PemeliharaanTable from "./PemeliharaanTable";

function Pemeliharaan() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Jenis Pemeliharaan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <PemeliharaanTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Pemeliharaan;
