import React, { useState, useEffect } from "react";
import { Modal, Table, FormControl, InputGroup, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import {
  sortCaret,
  headerSortingClasses,
  getSelectRowArsip,
} from "../../../../_metronic/_helpers";
import { Pagination } from "../../../helpers/pagination/Pagination";
import UsulanFormModal from "./UsulanFormModal";

function UsulanForm({ content, btnRef, saveForm }) {
  const history = useHistory();
  const [entities, setEntities] = useState([]);
  const [probisData, setProbisData] = useState([]);
  const [val, setVal] = useState();
  const [showAll, setShowAll] = useState(false);
  const [ids, setIds] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const [scrollPosition, setScrollPosition] = useState(0);
  const [showModal, setShowModal] = useState(false);

  const handleChange = (e) => {
    setVal(e.target.value);
  };

  useEffect(() => {
    setEntities([
      {
        id_arsip: "1",
        kode: "808080919191",
        judul: "SPT Tahunan | Budi Eko Prasetyo",
        ready: 1,
        tipe: "SPT Tahunan",
        tahun: "2019",
        // photo: "/media/users/user.png",
      },
      {
        id_arsip: "2",
        kode: "8021810109181",
        judul: "Pemindahbukuan | Tukul Adi Sutopo",
        ready: 1,
        tipe: "Pemindahbukuan",
        tahun: "2022",
        // photo: "/media/users/user.png",
      },
      {
        id_arsip: "3",
        kode: "82918191910191",
        judul: "Surat Ketetapan Pajak | Rinna Nanda Kusuma",
        ready: 1,
        tipe: "Surat Ketetapan Pajak",
        tahun: "2020",
        // photo: "/media/users/user.png",
      },
      {
        id_arsip: "4",
        kode: "87929289001091",
        judul: "Tagihan Pajak | Eko Prasetyo",
        ready: 0,
        tipe: "Tagihan Pajak",
        tahun: "2021",
        // photo: "/media/users/user.png",
      },
    ]);
  }, []);

  const handleSubmit = () => {
    // if (val === undefined || val === "") {
    //   setVal(undefined);
    //   getCasenameByArrayProbis(probisData).then(({ data }) => {
    //     setEntities(data);
    //     if (data.length > 1) {
    //       setShowAll(true);
    //     } else {
    //       setShowAll(false);
    //     }
    //   });
    // } else {
    //   getCasenameBySearch(val).then(({ data }) => {
    //     if (
    //       data.filter((item) => probisData.includes(item.id_probis)).length > 0
    //     ) {
    //       if (
    //         data.filter((item) => probisData.includes(item.id_probis)).length >
    //         1
    //       ) {
    //         setShowAll(true);
    //       } else {
    //         setShowAll(false);
    //       }
    //       setVal(undefined);
    //       setEntities(
    //         data.filter((item) => probisData.includes(item.id_probis))
    //       );
    //     } else {
    //       swal({
    //         title: "Data Tidak Ditemukan!",
    //         icon: "info",
    //         closeOnClickOutside: false,
    //       }).then((willApply) => {
    //         if (willApply) {
    //           setVal("");
    //           setEntities([]);
    //         }
    //       });
    //     }
    //   });
    // }
  };

  const saveSelectedArsip = () => {
    // if (!idMap) {
    //   saveMappingCasename({
    //     idCasename: ids,
    //     id_km_pro: id_km_pro,
    //   }).then(({ status }) => {
    //     if (status === 201 || status === 200) {
    //       swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
    //         history.push(`/dashboard`);
    //         history.replace(
    //           `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
    //         );
    //       });
    //     }
    //   });
    // } else {
    //   updateMappingCasename({
    //     idCasename: ids,
    //     id_km_pro: id_km_pro,
    //   }).then(({ status }) => {
    //     if (status === 201 || status === 200) {
    //       swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
    //         history.push(`/dashboard`);
    //         history.replace(
    //           `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
    //         );
    //       });
    //     }
    //   });
    // }
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "2px",
      },
    },
    {
      dataField: "kode",
      text: "Kode Arsip",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "250px",
      },
    },
    {
      dataField: "judul",
      text: "Judul Arsip",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "250px",
      },
    },
    {
      dataField: "tipe",
      text: "Tipe Arsip",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "250px",
      },
    },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_arsip",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "id_arsip", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: entities.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  const emptyDataMessage = () => {
    return <div className="text-center">Tidak Ada Data</div>;
  };
  const handleOpenModal = () => {

    if(ids.length > 0){
      // Save current scroll position
      setScrollPosition(window.scrollY);
      setShowModal(true);
    } else {
      swal({
        title: "Harap pilih arsip yang akan dipinjam terlebih dahulu!",
        // text: "Klik OK untuk melanjutkan",
        icon: "info",
        closeOnClickOutside: false,
      }).then((willApply) => {
        if (willApply) {
          // history.push("/logout");
        }
      })
    }
  };
  const handleCloseModal = () => {
    // Restore scroll position when modal is closed
    window.scrollTo(0, scrollPosition);
    setShowModal(false);
  };
  return (
    <>
      <>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <InputGroup className="mb-3">
              <FormControl
                name="arsip"
                aria-label="Default"
                placeholder="Masukkan Judul atau Kode Arsip"
                onChange={(e) => handleChange(e)}
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
          </div>

          <div className="col-lg-2 col-xl-2 mb-3">
            <Button type="submit" onClick={handleSubmit} variant="primary">
              Cari
            </Button>
          </div>
          <div className="col-lg-2 col-xl-2 mb-3">
            <Button
              type="submit"
              onClick={() => handleOpenModal()}
              ref={btnRef}
              variant="success"
              style={{ display: "none" }}
            >
              Simpan
            </Button>
          </div>
          {/* {ids.length > 0 ? (
              <div className="col-lg-2 col-xl-2 mb-3">
            <Button
                type="submit"
                onClick={()=>saveForm(ids)}
                ref={btnRef}
                variant="success"
                style={{ display: "none" }}
              >
                Simpan
              </Button>
            </div>
            ) :  null} */}
        </div>

        {/* <div className="row"> */}
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_arsip"
                  data={entities}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-12 col-xl-12 mb-3 mt-3">
                          <h4> Pilih Arsip </h4>
                        </div>
                      </div>
                      <div className="row"></div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        selectRow={getSelectRowArsip({
                          entities,
                          ids: ids,
                          setIds: setIds,
                        })}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <UsulanFormModal
        show={showModal}
        after={false}
        content={content}
        saveForm={saveForm}
        onHide={() => handleCloseModal()}
        onRef={() => {
          history.push(`/kelola/usulan/tambah`);
        }}
      />
      {/*Route to Open Tanggal Pinjam & Tanggal Kembali*/}
      {/* <Route path="/kelola/usulan/tambah/open">
        {({ history, match }) => (
          <UsulanModal
            show={match != null}
            // id={match && match.params.id}
            content={content}
            after={false}
            onHide={() => {
              history.push(
                `/kelola/usulan/tambah`
              );
            }}
            onRef={() => {
              history.push(
                `/kelola/usulan/tambah`
              );
            }}
          />
        )}
      </Route> */}
    </>
  );
}

export default UsulanForm;
