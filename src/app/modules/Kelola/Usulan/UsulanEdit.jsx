/* Library */
import React, { useEffect, useState, useRef } from "react";

/* Helper */
import { useSubheader } from "../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../_metronic/_partials/controls";
// import KdoForm from "./BmnForm";
// import BmnForm from "./BmnForm";
import {
  getBmnById,
  saveBmn,
  updateBmn,
  uploadFile,
} from "../../references/Api";
import swal from "sweetalert";
import UsulanForm from "./UsulanForm";

function UsulanEdit({
  history,
  match: {
    params: { id },
  },
}) {
  const initValues = {
    kode: "",
    judulArsip: "",
    tglPinjam: "",
    tglKembali: "",
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [content, setContent] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    let _title = id ? "Edit Usulan Peminjaman Arsip" : "Tambah Usulan Peminjaman Arsip";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      // getBmnById(id).then(({ data }) => {
      //   setContent({
      //     kode: data.kode,
      //     tahun: data.tahun,
      //     namaBmn: data.namaBmn,
      //     deskripsi: data.deskripsi,
      //     stock: data.stock,
      //     file_upload: data.photo,
      //   });
      // });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push("/kelola/usulan/pilih");
  };
  const enableLoading = () => {
    setLoading(true);
    setIsDisabled(true);
  };

  const disableLoading = () => {
    setLoading(false);
    setIsDisabled(false);
  };
  const saveForm = (values) => {
    // console.log(values)
    // if (!id) {
    //   enableLoading(true);
    //   const formData = new FormData();
    //   formData.append("file", values.file);
    //   uploadFile(formData).then(({ data }) => {
    //     disableLoading();
    //     saveBmn(
    //       values.kode,
    //       values.namaBmn,
    //       values.tahun,
    //       values.deskripsi,
    //       values.stock,
    //       data
    //     ).then(({ status }) => {
    //       if (status === 201 || status === 200) {
    //         swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
    //           history.push("/master/bmn");
    //         });
    //       } else {
    //         swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //           history.push("/master/bmn/tambah");
    //         });
    //       }
    //     });
    //   });
    // } else {
    //   if (values.file.name) {
    //     enableLoading();
    //     const formData = new FormData();
    //     formData.append("file", values.file);
    //     uploadFile(formData).then(({ data }) => {
    //       disableLoading();
    //       updateBmn(
    //         id,
    //         values.kode,
    //         values.namaBmn,
    //         values.tahun,
    //         values.deskripsi,
    //         values.stock,
    //         data
    //       ).then(({ status }) => {
    //         if (status === 201 || status === 200) {
    //           swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
    //             history.push("/master/bmn");
    //           });
    //         } else {
    //           swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //             history.push("/master/bmn/tambah");
    //           });
    //         }
    //       });
    //     });
    //   } else {
    //     updateBmn(
    //       id,
    //       values.kode,
    //       values.namaBmn,
    //       values.tahun,
    //       values.deskripsi,
    //       values.stock,
    //       values.file_upload
    //     ).then(({ status }) => {
    //       if (status === 201 || status === 200) {
    //         swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
    //           history.push("/master/bmn");
    //         });
    //       } else {
    //         swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //           history.push("/master/bmn/tambah");
    //         });
    //       }
    //     });
    //   }
    // }

    swal("Berhasil", "Usulan berhasil disimpan", "success").then(
      () => {
        history.push("/dashboard");
        history.replace("/kelola/usulan");
      }
    );
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <UsulanForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          {loading ? (
            <button
              type="button"
              className="btn btn-light ml-2"
              onClick={backAction}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              disabled={isDisabled}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
          ) : (
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
          )}
          {`  `}
          {loading ? (
            <button
              type="submit"
              className="btn btn-success spinner spinner-white spinner-left ml-2"
              onClick={saveButton}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              disabled={isDisabled}
            >
              <span>Simpan</span>
            </button>
          ) : (
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveButton}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              // disabled={disabled}
            >
              <i className="fas fa-paper-plane"></i>
              Pinjam
            </button>
          )}
        </div>
      </CardFooter>
    </Card>
  );
}

export default UsulanEdit;
