import React, { useState } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Input, Select as Sel, Textarea } from "../../../../helpers";
import {
  DatePickerFieldRowEnd,
  DatePickerFieldRowStart,
} from "../../../../helpers/form/DatePickerField";
function KdoForm({ saveForm, btnRef }) {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const initialValues = {
    startDate: "",
    endDate: "",
    namaPeminjam: "",
  };

  const ProposalEditSchema = Yup.object().shape({
    jenis: Yup.string().required("Jenis wajib diisi"),
    tahun: Yup.string().required("Tahun wajib diisi"),
    nomorPlat: Yup.string().required("No. Plat wajib diisi"),
    deskripsi: Yup.string().required("Deskripsi wajib diisi"),
    namaPeminjam: Yup.string().required("Nama Peminjam wajib diisi"),
  });
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
        }}
      >
        {({ handleSubmit, values }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Nama Peminjam */}
                <div className="form-group row">
                  <Field
                    name="namaPeminjam"
                    component={Input}
                    placeholder="Nama Peminjam"
                    label="Nama Peminjam"
                  />
                </div>
                {/* FIELD TANGGAL PINJAM&KEMBALI */}
                <div className="form-group row">
                  <DatePickerFieldRowStart
                    name="startDate"
                    label="Tanggal Pinjam"
                    show="true"
                    selected={startDate}
                    // setstartdate={(val)=>changeStartDate(val)}
                    enddate={endDate}
                    // onChange={()=>handleChangeStartDate(values.startDate)}
                    onChange={setStartDate(values.startDate)}
                    // onBlur={()=>handleChangeStartDate()}
                  />
                </div>
                {/* <div className="form-group row">
                  <label className="col-form-label">s/d</label>
                  </div> */}
                <div className="form-group row">
                  <DatePickerFieldRowEnd
                    name="endDate"
                    label="Tanggal Kembali"
                    show="true"
                    selected={endDate}
                    startdate={startDate}
                    onChange={setEndDate(values.endDate)}
                    // setenddate={setEndDate}
                  />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default KdoForm;
