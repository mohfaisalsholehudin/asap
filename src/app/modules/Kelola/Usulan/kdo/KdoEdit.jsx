import React, {useState} from "react";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";



import KdoForm from "./KdoForm";

function KdoEdit() {
  const history = useHistory();
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);


  const handleCancel = () => {
    history.push(`/kelola/usulan/pilih/kdo`);

  };

  const content = [
    {
      id: 1,
      peminjam: "Andi Rudi",
      waktu: "19-06-2024",
      status: "Dipinjam"
    },
    {
      id: 2,
      peminjam: "Budi Roso",
      waktu: "20-06-2024",
      status: "Akan Dipinjam"
    },
    {
      id: 3,
      peminjam: "Catur Novi",
      waktu: "24-06-2024",
      status: "Akan Dipinjam"
    },
  ]


  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      hidden: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        width: "100px",
      },
    },
    {
      dataField: "id",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "peminjam",
      text: "Peminjam",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "waktu",
      text: "Waktu",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    // {
    //   dataField: "action",
    //   text: "Aksi",
    //   footer: "",
    //   formatter: columnFormatters.ActionsColumnFormatterMasterAtkDetailPurchase,
    //   formatExtraData: {
    //     // openEditDialog: edit,
    //     openDeleteDialog: deleteAction,
    //     // publishKnowledge: apply,
    //     // showReview: review,
    //   },
    //   classes: "text-center pr-0",
    //   headerClasses: "text-center pr-3",
    //   style: {
    //     minWidth: "70px",
    //   },
    // },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "id", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };

  return (
    <>
      <div className="d-flex flex-row">
        <div
          className="flex-row-auto offcanvas-mobile w-350px w-xxl-500px"
          id="kt_profile_aside"
        >
          <div className="card card-custom card-stretch">
            <div className="card-body pt-4">
              <div className="py-5">
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <h5 className="font-weight-bold">INFO PEMINJAMAN</h5>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <div>
                    <img
                      src="https://asap.kanwildjpriau.com/storage/fileupload/kdo/kdo_170193.jpg"
                      alt=""
                      style={{
                        width: "100%",
                      }}
                    />
                  </div>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <h4>Jenis:</h4>
                  <h4 className="text-muted">Triton</h4>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  {/* <span className="font-weight-normal mr-2">Tahun:</span>
                    <span className="text-muted">2016</span> */}
                  <h4>Tahun:</h4>
                  <h4 className="text-muted">2016</h4>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <h4>Nomor Plat:</h4>
                  <h4 className="text-muted">BM 9344 AP</h4>
                </div>
              </div>
            </div>
            {/* <div className="card-body pt-4">
              <div className="d-flex align-items-center justify-content-between mb-2">
                    <h5 className="font-weight-bold">MASUKKAN ITEM</h5>
                  </div>
              <PembelianTambahForm 
                id={content.id}
              />
              </div> */}
          </div>
        </div>
        <div className="flex-row-fluid ml-lg-8">
          <div className="card card-custom card-stretch">
      
  {/* begin::Form */}
  <div className="form">
              {/* begin::Body */}
              <div className="card-body" style={{ paddingTop: "0px" }}>
                <div className="row" style={{ display: "block" }}>
                  <label className="col-xl-3"></label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ paddingLeft: "0px" }}
                  >
                    <h5 className="font-weight-bold mb-6">DATA PEMINJAMAN</h5>
                  </div>
                </div>
                {/* <div className="row"> */}
                  <>
                  <KdoForm />
                  </>
                {/* </div> */}
                <div className="row">
            <>
              <>
                <PaginationProvider pagination={paginationFactory(pagiOptions)}>
                  {({ paginationProps, paginationTableProps }) => {
                    return (
                      <>
                        <ToolkitProvider
                          keyField="id"
                          data={content}
                          columns={columns}
                          search
                        >
                          {(props) => (
                            <div
                              className="col-lg-12"
                              style={{
                                paddingRight: "0px",
                                paddingLeft: "0px",
                              }}
                            >
                              <BootstrapTable
                                {...props.baseProps}
                                wrapperClasses="table-responsive"
                                bordered={false}
                                headerWrapperClasses="thead-light"
                                classes="table table-condensed table-head-custom table-vertical-center overflow-hidden"
                                defaultSorted={defaultSorted}
                                bootstrap4
                                {...paginationTableProps}
                              ></BootstrapTable>
                              {/* <Pagination
                        paginationProps={paginationProps}
                        //   isLoading={loading}
                      /> */}
                            </div>
                          )}
                        </ToolkitProvider>
                      </>
                    );
                  }}
                </PaginationProvider>
              </>
            </>
          </div>
                <div
                  className="row mt-3"
                  style={{ display: "block", textAlign: "right" }}
                >
                  <button
                    type="submit"
                    className="btn btn-danger"
                      onClick={() => handleCancel()}
                    disabled={false}
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      // paddingRight: "0px", paddingLeft: "0px"
                    }}
                    // disabled={disabled}
                  >
                    <i className="far fa-times-circle"></i>
                    Batalkan
                  </button>
                  <button
                    type="submit"
                    className="btn btn-success ml-2"
                    // onClick={() => handleSavePurchaseAtk()}
                    disabled={false}
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      // paddingRight: "0px", paddingLeft: "0px"
                    }}
                    // disabled={disabled}
                  >
                    <i className="fas fa-save"></i>
                    Simpan
                  </button>
                </div>
              </div>
              {/* end::Body */}
            </div>
            {/* end::Form */}
         
        
          
          </div>
        </div>
      </div>
    </>
  );
}

export default KdoEdit;
