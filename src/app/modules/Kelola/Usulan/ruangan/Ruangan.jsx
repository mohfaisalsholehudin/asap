/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react-hooks/exhaustive-deps */
/* Library */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";

import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { getKdo, getRuangan } from "../../../references/Api";

/* Utility */

function Ruangan({
  history,
  match: {
    params: { id },
  },
}) {
  const [content, setContent] = useState([]);
  const [data, setData] = useState([]);
  const [searchTxt, setSearchTxt] = useState("");
  const [currentPage, setCurrentPage] = useState(0);
  const [sizePage, setSizePage] = useState(10);
  const [loading, setLoading] = useState(true);

  const edit = (id) => history.push(`/master/kdo/${id}/edit`);

  const backAction = () => {
    history.push("/kelola/usulan/pilih");
  };

  useEffect(() => {
    getRuangan(
      currentPage === 0 ? currentPage : currentPage - 1,
      sizePage,
      "id",
      "asc",
      searchTxt ? searchTxt : ""
    ).then(({ data }) => {
      if (data.content.length > 0) {
        setLoading(false);
        setContent(data.content);
        setData(data);
      } else {
        setLoading(false);
        swal({
          title: "Data Tidak Tersedia!",
          icon: "info",
          closeOnClickOutside: false,
        }).then((willApply) => {
          if (willApply) {
            // history.push("/logout");
          }
        });
      }
    });
  }, [currentPage, sizePage]);

  const handlePinjam = (val) => {
    history.push(`/kelola/usulan/pilih/ruangan/${val}/pinjam`)
  }

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Ruangan"
          style={{ backgroundColor: "#FFC91B" }}
        >
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
          </CardHeaderToolbar>
        </CardHeader>
        {/* <CardBody>
        </CardBody> */}
      </Card>
      <>
        <div className="row">
          <div className="col-xl-4 col-lg-6 col-md-6 col-sm-6">
            <div className="card card-custom gutter-b card-stretch">
              <div className="card-body pt-4">
                <div className="d-flex align-items-center mb-7">
                  <div className="flex-shrink-0 mr-4">
                    <div>
                      <img
                        src="https://asap.kanwildjpriau.com/storage/fileupload/room/room_745824.jpg"
                        alt=""
                        style={{
                          width: "50%",
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div className="mb-7">
                  <div className="d-flex justify-content-between align-items-center">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Nama:
                    </span>
                    <a href="#" className="text-muted text-hover-primary">
                      Hang Nadim
                    </a>
                  </div>
                  <div className="d-flex justify-content-between align-items-cente my-1">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Kapasitas:
                    </span>
                    <a href="#" className="text-muted text-hover-primary">
                      15 orang
                    </a>
                  </div>
                  <div className="d-flex justify-content-between align-items-center">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Lokasi:
                    </span>
                    <span className="text-muted font-weight-bold">
                      Lantai 1
                    </span>
                  </div>
                </div>
                {/* <a
                  href="#"
                  className="btn btn-block btn-sm btn-light-primary font-weight-bolder text-uppercase py-4"
                >
                  Pinjam Sekarang
                </a> */}
                  <button
                  type="button"
                  className="btn btn-block btn-sm btn-light-primary font-weight-bolder text-uppercase py-4"
                    onClick={()=>handlePinjam(1)}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                >
                  <span>Pinjam Sekarang</span>
                </button>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-6 col-md-6 col-sm-6">
            <div className="card card-custom gutter-b card-stretch">
              <div className="card-body pt-4">
                <div className="d-flex align-items-center mb-7">
                  <div className="flex-shrink-0 mr-4">
                    <div>
                      <img
                        src="https://asap.kanwildjpriau.com/storage/fileupload/room/room_256161.jpg"
                        alt=""
                        style={{
                          width: "50%",
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div className="mb-7">
                  <div className="d-flex justify-content-between align-items-center">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Nama:
                    </span>
                    <a href="#" className="text-muted text-hover-primary">
                      Hang Lekiu
                    </a>
                  </div>
                  <div className="d-flex justify-content-between align-items-cente my-1">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Kapasitas:
                    </span>
                    <a href="#" className="text-muted text-hover-primary">
                      25 Orang
                    </a>
                  </div>
                  <div className="d-flex justify-content-between align-items-center">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Lokasi:
                    </span>
                    <span className="text-muted font-weight-bold">
                      Lantai 2
                    </span>
                  </div>
                </div>
                {/* <a
                      href="#"
                      className="btn btn-block btn-sm btn-light-danger font-weight-bolder text-uppercase py-4"
                    >
                      Sedang Dipinjam
                    </a> */}
                <button
                  type="button"
                  className="btn btn-block btn-sm btn-light-primary font-weight-bolder text-uppercase py-4"
                  //   onClick={saveButton}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={true}
                >
                  <span>Pinjam Sekarang</span>
                </button>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-6 col-md-6 col-sm-6">
            <div className="card card-custom gutter-b card-stretch">
              <div className="card-body pt-4">
                <div className="d-flex align-items-center mb-7">
                  <div className="flex-shrink-0 mr-4">
                    <div>
                      <img
                        src="https://asap.kanwildjpriau.com/storage/fileupload/room/room_466536.jpg"
                        alt=""
                        style={{
                          width: "50%",
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div className="mb-7">
                  <div className="d-flex justify-content-between align-items-center">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Nama:
                    </span>
                    <a href="#" className="text-muted text-hover-primary">
                      Ruang Rapat Bagian Umum
                    </a>
                  </div>
                  <div className="d-flex justify-content-between align-items-cente my-1">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Kapasitas:
                    </span>
                    <a href="#" className="text-muted text-hover-primary">
                      10 Orang
                    </a>
                  </div>
                  <div className="d-flex justify-content-between align-items-center">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Lokasi:
                    </span>
                    <span className="text-muted font-weight-bold">
                      Lantai 2
                    </span>
                  </div>
                </div>
                <button
                  type="button"
                  className="btn btn-block btn-sm btn-light-primary font-weight-bolder text-uppercase py-4"
                  //   onClick={saveButton}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                >
                  <span>Pinjam Sekarang</span>
                </button>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-6 col-md-6 col-sm-6">
            <div className="card card-custom gutter-b card-stretch">
              <div className="card-body pt-4">
                <div className="d-flex align-items-center mb-7">
                  <div className="flex-shrink-0 mr-4">
                    <div>
                      <img
                        src="https://asap.kanwildjpriau.com/storage/fileupload/room/room_411449.jpg"
                        alt=""
                        style={{
                          width: "50%",
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div className="mb-7">
                  <div className="d-flex justify-content-between align-items-center">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Nama:
                    </span>
                    <a href="#" className="text-muted text-hover-primary">
                      Hang Jebat
                    </a>
                  </div>
                  <div className="d-flex justify-content-between align-items-cente my-1">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Kapasitas:
                    </span>
                    <a href="#" className="text-muted text-hover-primary">
                      30 Orang
                    </a>
                  </div>
                  <div className="d-flex justify-content-between align-items-center">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Lokasi:
                    </span>
                    <span className="text-muted font-weight-bold">
                      Lantai 3
                    </span>
                  </div>
                </div>
                <button
                  type="button"
                  className="btn btn-block btn-sm btn-light-primary font-weight-bolder text-uppercase py-4"
                  //   onClick={saveButton}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                >
                  <span>Pinjam Sekarang</span>
                </button>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-6 col-md-6 col-sm-6">
            <div className="card card-custom gutter-b card-stretch">
              <div className="card-body pt-4">
                <div className="d-flex align-items-center mb-7">
                  <div className="flex-shrink-0 mr-4">
                    <div>
                      <img
                        src="https://asap.kanwildjpriau.com/storage/fileupload/room/room_119503.jpg"
                        alt=""
                        style={{
                          width: "50%",
                        }}
                      />
                    </div>
                  </div>
                </div>
                <div className="mb-7">
                  <div className="d-flex justify-content-between align-items-center">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Nama:
                    </span>
                    <a href="#" className="text-muted text-hover-primary">
                      Hang Tuah
                    </a>
                  </div>
                  <div className="d-flex justify-content-between align-items-cente my-1">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Kapasitas:
                    </span>
                    <a href="#" className="text-muted text-hover-primary">
                      50 Orang
                    </a>
                  </div>
                  <div className="d-flex justify-content-between align-items-center">
                    <span className="text-dark-75 font-weight-bolder mr-2">
                      Lokasi:
                    </span>
                    <span className="text-muted font-weight-bold">
                      Lantai 3
                    </span>
                  </div>
                </div>
                <button
                  type="button"
                  className="btn btn-block btn-sm btn-light-primary font-weight-bolder text-uppercase py-4"
                  //   onClick={saveButton}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                >
                  <span>Pinjam Sekarang</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    </>
  );
}

export default Ruangan;
