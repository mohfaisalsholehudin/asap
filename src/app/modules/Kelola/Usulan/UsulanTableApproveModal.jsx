import React, { useEffect, useState } from "react";
import { Modal, Table, FormControl, InputGroup, Button } from "react-bootstrap";
import SVG from "react-inlinesvg";
import * as Yup from "yup";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
// import { useSubheader } from "../../../../../../../_metronic/layout";
import { Field, Formik, Form } from "formik";
import {
  DatePickerFieldRowEnd,
  DatePickerFieldRowStart,
} from "../../../helpers/form/DatePickerField";
import { Textarea } from "../../../helpers";

function UsulanTableApproveModal({ id, show, content, onHide, saveForm }) {
  const history = useHistory();
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [isShow, setIsShow] = useState(true);
  const [isReject, setIsReject] = useState(false);
  //   const [title, setTitle] = useState("");

  //   useEffect(() => {
  //     let _title = id ? "Edit Peraturan Terkait" : "Tambah Peraturan Terkait";

  //     setTitle(_title);
  //     suhbeader.setTitle(_title);
  //   }, [id, suhbeader]);

  // const initialValues = {
  //   pokok_pengaturan: "",
  //   jenis: ""
  // };

  const validationSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Nama is required"),
  });

  const initValues = {
    startDate: "",
    endDate: "",
  };

  const initialValues = {
    alasan_penolakan: "",
  };

  const rejectSchema = Yup.object().shape({
    alasan_penolakan: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Alasan is required"),
  });

  const handleApprove = () => {
    swal("Berhasil", "Usulan berhasil disetujui", "success").then(() => {
      history.push("/dashboard");
      history.replace("/kelola/usulan");
    });
  };
  const handleReject = () => {
    setIsShow(false);
    setIsReject(true);
  };

  const handleSaveReject = () => {
    swal("Berhasil", "Usulan berhasil ditolak", "success").then(() => {
      setIsShow(true);
      setIsReject(false);
      history.push("/dashboard");
      history.replace("/kelola/usulan");
    });
  };
  const handleCancel = () => {
    setIsReject(false);
    setIsShow(true);
  };

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detail Usulan Peminjaman Arsip
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row mb-2">
          <div className="col-md-3 col-lg-3 col-lg-3 pl-0">
            <span className="font-weight-normal mr-2">Nama Peminjam :</span>
          </div>
          <div className="col-md-9 col-lg-9 col-lg-9">
            <span className="text-muted">Andika Perkasa</span>
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-md-3 col-lg-3 col-lg-3 pl-0">
            <span className="font-weight-normal mr-2">Tanggal Pinjam :</span>
          </div>
          <div className="col-md-9 col-lg-9 col-lg-9">
            <span className="text-muted">08-05-2023</span>
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-md-3 col-lg-3 col-lg-3 pl-0">
            <span className="font-weight-normal mr-2">Tanggal Kembali :</span>
          </div>
          <div className="col-md-9 col-lg-9 col-lg-9">
            <span className="text-muted">15-05-2023</span>
          </div>
        </div>
        <div className="row mb-7">
          <div className="col-md-3 col-lg-3 col-lg-3 pl-0">
            <span className="font-weight-normal mr-2">
              Lokasi Penyimpanan :
            </span>
          </div>
          <div className="col-md-9 col-lg-9 col-lg-9">
            <span className="text-muted">E-3-IV-2</span>
          </div>
        </div>
        {/* <div className="py-5">
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-normal mr-2">Nama Peminjam:</span>
              <span className="text-muted text-hover-primary">
                Andika Perkasa
              </span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-normal mr-2">Tanggal Pinjam:</span>
              <span className="text-muted">08-05-2023</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-normal mr-2">Tanggal Kembali:</span>
              <span className="text-muted">15-05-2023</span>
            </div>
          </div> 
        </div> */}
        <div className="row">
          <Table responsive bordered hover>
            <thead
              style={{
                // border: "1px solid #3699FF",
                textAlign: "center",
              }}
            >
              <tr>
                <th>No</th>
                <th style={{ textAlign: "left" }}>Kode</th>
                <th style={{ textAlign: "left" }}>Judul Arsip</th>
                {/* <th>Aksi</th> */}
              </tr>
            </thead>
            <tbody
              style={{
                // border: "1px solid #3699FF",
                textAlign: "center",
              }}
            >
              <tr style={{ height: "40px" }}>
                <td>1</td>
                <td style={{ textAlign: "left" }}>808080919191</td>
                <td style={{ textAlign: "left" }}>
                  SPT Tahunan | Budi Eko Prasetyo
                </td>
              </tr>
              <tr style={{ height: "40px" }}>
                <td>2</td>
                <td style={{ textAlign: "left" }}>8021810109181</td>
                <td style={{ textAlign: "left" }}>
                  Pemindahbukuan | Tukul Adi Sutopo
                </td>
              </tr>
              {/* {perterkait.map((data, index) => (
                <tr key={index} style={{ height: "40px" }}>
                  <td>{index + 1}</td>
                  <td style={{ textAlign: "left" }}>{data.no_regulasi}</td>
                  <td style={{ textAlign: "left" }}>{data.perihal}</td>
                  <td>
                    <a
                      title="Tambah"
                      className="btn btn-icon btn-light btn-hover-success btn-sm mx-3"
                      onClick={() => addPer(data.id_peraturan)}
                    >
                      <span className="svg-icon svg-icon-md svg-icon-success">
                        <SVG
                          src={toAbsoluteUrl(
                            "/media/svg/icons/Navigation/Plus.svg"
                          )}
                        />
                      </span>
                    </a>
                  </td>
                </tr>
              ))} */}
            </tbody>
            {/* <tfoot style={{ border: '1px solid #3699FF', textAlign: 'center' }}>
                            <tr style={{ height: '40px' }}>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot> */}
          </Table>
        </div>

        {isReject ? (
          <>
            <Formik
              enableReinitialize={true}
              initialValues={initialValues}
              validationSchema={rejectSchema}
              onSubmit={(values) => {
                //  console.log(values);
                handleSaveReject(values);
              }}
            >
              {({ handleSubmit }) => {
                return (
                  <Form className="form form-label-right">
                    <div
                      className="form-group row"
                      style={{ marginBottom: "0px" }}
                    >
                      <div className="col-lg-9 col-xl-6">
                        <h5 className="mt-6" style={{ fontWeight: "600" }}>
                          ALASAN PENOLAKAN
                        </h5>
                      </div>
                    </div>
                    <div className="form-group row">
                      <Field
                        name="alasan_penolakan"
                        component={Textarea}
                        placeholder="Alasan Penolakan"
                        custom={"custom"}
                      />
                    </div>
                    <div className="col-lg-12" style={{ textAlign: "center" }}>
                      <button
                        type="button"
                        onClick={handleCancel}
                        className="btn btn-light"
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                      >
                        <i className="flaticon2-cancel icon-nm"></i>
                        Batal
                      </button>
                      {`  `}
                      <button
                        type="submit"
                        onSubmit={() => handleSubmit()}
                        className="btn btn-success ml-2"
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                      >
                        <i className="fas fa-check"></i>
                        Kirim
                      </button>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </>
        ) : null}
        {isShow ? (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              className="btn btn-light ml-2"
              onClick={() => onHide()}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              //   disabled={isDisabled}
            >
              <i className="fas fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="button"
              className="btn btn-danger ml-2"
              onClick={() => handleReject()}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              //   disabled={isDisabled}
            >
              <i className="flaticon2-cancel icon-nm"></i>
              Tolak
            </button>
            <button
              type="button"
              className="btn btn-success ml-2"
              onClick={() => handleApprove()}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              //   disabled={isDisabled}
            >
              <i className="fas fa-check"></i>
              Setuju
            </button>
          </div>
        ) : null}
      </Modal.Body>
    </Modal>
  );
}

export default UsulanTableApproveModal;
