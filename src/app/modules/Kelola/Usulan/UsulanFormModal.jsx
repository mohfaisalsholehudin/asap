import React, { useEffect, useState } from "react";
import { Modal, Table, FormControl, InputGroup, Button } from "react-bootstrap";
import SVG from "react-inlinesvg";
import * as Yup from "yup";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
// import { useSubheader } from "../../../../../../../_metronic/layout";
import { Field, Formik, Form } from "formik";
import {
  DatePickerFieldRowEnd,
  DatePickerFieldRowStart,
} from "../../../helpers/form/DatePickerField";

function UsulanFormModal({ id, show, content, onHide, saveForm }) {
  const history = useHistory();
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  //   const [title, setTitle] = useState("");

  //   useEffect(() => {
  //     let _title = id ? "Edit Peraturan Terkait" : "Tambah Peraturan Terkait";

  //     setTitle(_title);
  //     suhbeader.setTitle(_title);
  //   }, [id, suhbeader]);

  // const initialValues = {
  //   pokok_pengaturan: "",
  //   jenis: ""
  // };

  const validationSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Nama is required"),
  });

  const initValues = {
    // id_tipe_km: "",
    startDate: "",
    endDate: "",
    // id_sektor: "",
    // id_probis: "",
    // id_csname: "",
    // id_subcase: "",
    // key: ""
  };
  const savePengaturan = (values) => {};

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Daftar Usulan Peminjaman Arsip
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <Table responsive bordered hover>
            <thead
              style={{
                // border: "1px solid #3699FF",
                textAlign: "center",
              }}
            >
              <tr>
                <th>No</th>
                <th style={{ textAlign: "left" }}>Kode</th>
                <th style={{ textAlign: "left" }}>Judul Arsip</th>
                {/* <th>Aksi</th> */}
              </tr>
            </thead>
            <tbody
              style={{
                // border: "1px solid #3699FF",
                textAlign: "center",
              }}
            >
              <tr style={{ height: "40px" }}>
                <td>1</td>
                <td style={{ textAlign: "left" }}>808080919191</td>
                <td style={{ textAlign: "left" }}>
                  SPT Tahunan | Budi Eko Prasetyo
                </td>
              </tr>
              <tr style={{ height: "40px" }}>
                <td>2</td>
                <td style={{ textAlign: "left" }}>8021810109181</td>
                <td style={{ textAlign: "left" }}>
                  Pemindahbukuan | Tukul Adi Sutopo
                </td>
              </tr>
              {/* {perterkait.map((data, index) => (
                <tr key={index} style={{ height: "40px" }}>
                  <td>{index + 1}</td>
                  <td style={{ textAlign: "left" }}>{data.no_regulasi}</td>
                  <td style={{ textAlign: "left" }}>{data.perihal}</td>
                  <td>
                    <a
                      title="Tambah"
                      className="btn btn-icon btn-light btn-hover-success btn-sm mx-3"
                      onClick={() => addPer(data.id_peraturan)}
                    >
                      <span className="svg-icon svg-icon-md svg-icon-success">
                        <SVG
                          src={toAbsoluteUrl(
                            "/media/svg/icons/Navigation/Plus.svg"
                          )}
                        />
                      </span>
                    </a>
                  </td>
                </tr>
              ))} */}
            </tbody>
            {/* <tfoot style={{ border: '1px solid #3699FF', textAlign: 'center' }}>
                            <tr style={{ height: '40px' }}>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot> */}
          </Table>
        </div>
        <div className="row">
          <div className="col-lg-12 col-xl-12">
            <Formik
              enableReinitialize={true}
              initialValues={initValues}
              validationSchema={validationSchema}
              onSubmit={(values) => {
                console.log(values);
                //   saveForm(values);
              }}
            >
              {({
                handleSubmit,
                handleClick,
                setFieldValue,
                handleBlur,
                handleChange,
                errors,
                touched,
                values,
                isValid,
              }) => {
                // const handleChangeStartDate = () => {
                //   // changeStartDate(values.tgl_knowledge_start)
                //   // console.log(values.tgl_knowledge_start)
                //   console.log(values.tgl_knowledge_start)
                // }
                const handleClickSubmit = (val) => {
                    saveForm(val)
                }
                return (
                  <>
                    <Form className="form form-label-right">
                      {/* FIELD TANGGAL PINJAM&KEMBALI */}
                      <div className="form-group row">
                        <DatePickerFieldRowStart
                          name="startDate"
                          label="Tanggal Pinjam"
                          show="true"
                          selected={startDate}
                          // setstartdate={(val)=>changeStartDate(val)}
                          enddate={endDate}
                          // onChange={()=>handleChangeStartDate(values.startDate)}
                          onChange={setStartDate(values.startDate)}
                          // onBlur={()=>handleChangeStartDate()}
                        />
                      </div>
                      {/* <div className="form-group row">
                  <label className="col-form-label">s/d</label>
                  </div> */}
                      <div className="form-group row">
                        <DatePickerFieldRowEnd
                          name="endDate"
                          label="Tanggal Kembali"
                          show="true"
                          selected={endDate}
                          startdate={startDate}
                          onChange={setEndDate(values.endDate)}
                          // setenddate={setEndDate}
                        />
                      </div>
                    </Form>
                    <div className="col-lg-12" style={{ textAlign: "center" }}>
                      <button
                        type="button"
                        className="btn btn-light ml-2"
                          onClick={()=> onHide()}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                        //   disabled={isDisabled}
                      >
                        <i className="fa fa-arrow-left"></i>
                        Kembali
                      </button>
                      <button
                        type="submit"
                        className="btn btn-success ml-2"
                        // onSubmit={handleSubmit}
                          onClick={()=>handleClickSubmit(values)}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                        // disabled={disabled}
                      >
                        <i className="fas fa-save"></i>
                        Ajukan Usulan
                      </button>
                    </div>
                  </>
                );
              }}
            </Formik>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default UsulanFormModal;
