import React, { useEffect, useState } from "react";
import { Modal, Table, FormControl, InputGroup, Button } from "react-bootstrap";
import SVG from "react-inlinesvg";
import * as Yup from "yup";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
// import { useSubheader } from "../../../../../../../_metronic/layout";
import { Field, Formik, Form } from "formik";
import {
  DatePickerField,
  DatePickerFieldRowEnd,
  DatePickerFieldRowStart,
} from "../../../helpers/form/DatePickerField";
import { Textarea } from "../../../helpers";

function KembaliDetailTableModal({ id, show, content, onHide, saveForm }) {
  const history = useHistory();
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  //   const [title, setTitle] = useState("");

  //   useEffect(() => {
  //     let _title = id ? "Edit Peraturan Terkait" : "Tambah Peraturan Terkait";

  //     setTitle(_title);
  //     suhbeader.setTitle(_title);
  //   }, [id, suhbeader]);

  // const initialValues = {
  //   pokok_pengaturan: "",
  //   jenis: ""
  // };

  const validationSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Nama is required"),
  });

  const initValues = {
    // id_tipe_km: "",
    tglPengembalian: "",
    catatan: "",
    // id_probis: "",
    // id_csname: "",
    // id_subcase: "",
    // key: ""
  };
  const savePengaturan = (values) => {};

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detail Pengembalian
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-12 col-xl-12">
            <Formik
              enableReinitialize={true}
              initialValues={initValues}
              validationSchema={validationSchema}
              onSubmit={(values) => {
                // console.log(values);
                  saveForm(values);
              }}
            >
              {({
                handleSubmit,
                handleClick,
                setFieldValue,
                handleBlur,
                handleChange,
                errors,
                touched,
                values,
                isValid,
              }) => {
                // const handleChangeStartDate = () => {
                //   // changeStartDate(values.tgl_knowledge_start)
                //   // console.log(values.tgl_knowledge_start)
                //   console.log(values.tgl_knowledge_start)
                // }
                const handleClickSubmit = (val) => {
                    saveForm(val)
                }
                return (
                  <>
                    <Form className="form form-label-right">
                    <div className="form-group row">
                      <Field
                        name="catatan"
                        component={Textarea}
                        placeholder="Catatan Pengembalian"
                        label="Catatan Pengembalian"
                        // custom={"custom"}
                      />
                    </div>
                      {/* FIELD TANGGAL PINJAM&KEMBALI */}
                      <div className="form-group row">
                    <DatePickerField name="tglPengembalian" label="Tanggal Pengembalian" />
                  </div>
                    </Form>
                    <div className="col-lg-12" style={{ textAlign: "center" }}>
                      <button
                        type="button"
                        className="btn btn-light ml-2"
                          onClick={()=> onHide()}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                        //   disabled={isDisabled}
                      >
                        <i className="fa fa-arrow-left"></i>
                        Kembali
                      </button>
                      <button
                        type="submit"
                        className="btn btn-success ml-2"
                        // onSubmit={handleSubmit}
                          onClick={()=>handleClickSubmit(values)}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                        // disabled={disabled}
                      >
                        <i className="fas fa-save"></i>
                        Simpan
                      </button>
                    </div>
                  </>
                );
              }}
            </Formik>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default KembaliDetailTableModal;
