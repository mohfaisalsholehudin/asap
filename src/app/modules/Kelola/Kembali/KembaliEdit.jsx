import React from 'react'
import KembaliDetailTable from './KembaliDetailTable';




function KembaliEdit() {


    const dateFormat = () => {
        const date = new Date();
        const tanggal = String(date.getDate()).padStart(2, '0');
        var bulan = String(date.getMonth() + 1).padStart(2, '0');
        const tahun = date.getFullYear();
        return tanggal + "-" + bulan + "-" + tahun ;
      }



  return (
    <>
    <>
      <div className="d-flex flex-row">
        <div
          className="flex-row-auto offcanvas-mobile w-350px w-xxl-500px"
          id="kt_profile_aside"
        >
          <div className="card card-custom card-stretch">
            {/* <div className="card-header py-3">
              <div className="card-title align-items-start flex-column">
                <h3 className="card-label font-weight-bolder text-dark">
                  ATK Baru
                </h3>
              </div>
            </div> */}
            <div className="card-body pt-4">
              <div className="py-5">
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <h5 className="font-weight-bold">DATA PEMINJAM</h5>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <span className="font-weight-normal mr-2">Nama:</span>
                  <span className="text-muted text-hover-primary">Heri Saputra</span>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <span className="font-weight-normal mr-2">Tanggal Pinjam:</span>
                  <span className="text-muted">{dateFormat()}</span>
                </div>
                <div className="d-flex align-items-center justify-content-between mb-2">
                  <span className="font-weight-normal mr-2">
                    Tanggal Kembali:
                  </span>
                  <span className="text-muted">{dateFormat()}</span>
                </div>
                <div className="d-flex align-items-center justify-content-between">
                  <span className="font-weight-normal mr-2">Lokasi Penyimpanan:</span>
                  <span className="text-muted">E-3-IV-2</span>
                  {/* <span className="text-muted">2023-12-06 14:05:21</span> */}
                </div>
              </div>
            </div>
            {/* <div className="card-body pt-4">
            <div className="d-flex align-items-center justify-content-between mb-2">
                  <h5 className="font-weight-bold">MASUKKAN ITEM</h5>
                </div>
            <PembelianTambahForm 
              id={content.id}
            />
            </div> */}
          </div>
        </div>
        <div className="flex-row-fluid ml-lg-8">
          <KembaliDetailTable
            // id={content.id}
          />
        </div>
      </div>
    </>
  </>
  )
}

export default KembaliEdit