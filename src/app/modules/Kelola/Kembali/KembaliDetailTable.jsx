import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";

import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";

/* Helpers */
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import {
  cancelPurchaseAtk,
  checkAtkEmpty,
  deleteDetailPurchaseAtk,
  savePurchaseAtk,
} from "../../references/Api";
import KembaliDetailTableModal from "./KembaliDetailTableModal";
function KembaliDetailTable({ id }) {
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const [content, setContent] = useState([]);
  const [scrollPosition, setScrollPosition] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const history = useHistory();

  useEffect(() => {
    // checkAtkEmpty().then(({ data }) => {
    //   if (data.length > 0) {
    //     data[0] ? setContent(data[0].atkPurchaseDetails) : setContent(data[0]);
    //   }
    // });

    setContent([
        {
            id: 1,
            kode: "808080919191",
            judul: "SPT Tahunan | Budi Eko Prasetyo",

        },
        {
            id: 2,
            kode: "8021810109181",
            judul: "Pemindahbukuan | Tukul Adi Sutopo",

        }
    ])
  }, []);

  const deleteAction = (id) => {
    deleteDetailPurchaseAtk(id).then(({ status }) => {
      if (status === 200) {
        history.push("/dashboard");
        history.replace("/master/atk/pembelian/tambah");
      } else {
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.replace("/master/atk/pembelian/tambah");
        });
      }
    });
  };

  const handleReturn = () => {
    handleOpenModal()
  }

  const handleBack = () => {
    history.push(
        `/kelola/pengembalian`
      );
  }

  const handleOpenModal = () => {

    setScrollPosition(window.scrollY);
    setShowModal(true);
  };
  const handleCloseModal = () => {
    // Restore scroll position when modal is closed
    window.scrollTo(0, scrollPosition);
    setShowModal(false);
  };

  const saveForm = () => {
    swal("Berhasil", "Arsip berhasil dikembalikan", "success").then(
        () => {
          history.push("/dashboard");
          history.replace(`/kelola/pengembalian/${id}/detail`);
        }
      );
  }

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
    //   hidden: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        width: "100px",
      },
    },
    {
      dataField: "id",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "kode",
      text: "Kode",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      //   style: {
      //     width: "30px",
      //   },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "judul",
      text: "Judul Arsip",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,

      //   style: {
      //     width: "50px",
      //   },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
   
    // {
    //   dataField: "stock",
    //   text: "Stock",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   formatter: columnFormatters.StockColumnFormatterMasterAtk,
    //   style: {
    //     width: "80px",
    //   },
    // },
    // {
    //   dataField: "status",
    //   text: "Status",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   hidden: tab === 'selesai' || tab === 'tolak' ? true : false,
    //   formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeUsulanMatrix,
    // },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterKelolaKembaliDetail,
      formatExtraData: {
        // openEditDialog: edit,
        openReturnDialog: handleReturn,
        // publishKnowledge: apply,
        // showReview: review,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "70px",
      },
    },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "id", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };

  const handleCancelPurchaseAtk = () => {
    cancelPurchaseAtk(id).then(({ status }) => {
      if (status === 200) {
        swal("Berhasil", "Pembelian berhasil dibatalkan", "success").then(
          () => {
            history.push("/dashboard");
            history.replace(`/master/atk/pembelian`);
          }
        );
      } else {
        swal("Gagal", "Pembelian gagal dibatalkan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/master/atk/pembelian/tambah`);
        });
      }
    });
  };
  const handleSavePurchaseAtk = () => {
    savePurchaseAtk(id).then(({ status }) => {
      if (status === 200) {
        swal("Berhasil", "Pembelian berhasil disimpan", "success").then(() => {
          history.push("/dashboard");
          history.replace(`/master/atk/pembelian`);
        });
      } else {
        swal("Gagal", "Pembelian gagal disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/master/atk/pembelian/tambah`);
        });
      }
    });
  };

  return (
    <>
    <div className="card card-custom card-stretch">
      {/* begin::Form */}
      <div className="form">
        {/* begin::Body */}
        <div className="card-body" style={{ paddingTop: "0px" }}>
          <div className="row" style={{ display: "block" }}>
            <label className="col-xl-3"></label>
            <div className="col-lg-12 col-xl-12" style={{ paddingLeft: "0px" }}>
              <h5 className="font-weight-bold mb-6">DAFTAR ARSIP YANG BELUM DIKEMBALIKAN</h5>
            </div>
          </div>
          <div className="row">
            <>
              <>
                <PaginationProvider pagination={paginationFactory(pagiOptions)}>
                  {({ paginationProps, paginationTableProps }) => {
                    return (
                      <>
                        <ToolkitProvider
                          keyField="id"
                          data={content}
                          columns={columns}
                          search
                        >
                          {(props) => (
                            <div
                              className="col-lg-12"
                              style={{
                                paddingRight: "0px",
                                paddingLeft: "0px",
                              }}
                            >
                              <BootstrapTable
                                {...props.baseProps}
                                wrapperClasses="table-responsive"
                                bordered={false}
                                headerWrapperClasses="thead-light"
                                classes="table table-condensed table-head-custom table-vertical-center overflow-hidden"
                                defaultSorted={defaultSorted}
                                bootstrap4
                                {...paginationTableProps}
                              ></BootstrapTable>
                              {/* <Pagination
                        paginationProps={paginationProps}
                        //   isLoading={loading}
                      /> */}
                            </div>
                          )}
                        </ToolkitProvider>
                      </>
                    );
                  }}
                </PaginationProvider>
              </>
            </>
          </div>
          <div
            className="row mt-3"
            style={{ display: "block", textAlign: "right" }}
          >
            <button
              type="button"
              className="btn btn-danger"
              onClick={() => handleBack()}
              disabled={false}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                // paddingRight: "0px", paddingLeft: "0px"
              }}
              // disabled={disabled}
            >
              <i className="far fa-times-circle"></i>
              Batal
            </button>
          </div>
        </div>
        {/* end::Body */}
      </div>
      {/* end::Form */}
    </div>
    <KembaliDetailTableModal
        show={showModal}
        after={false}
        content={content}
        saveForm={saveForm}
        onHide={() => handleCloseModal()}
        onRef={() => {
          history.push(`/kelola/usulan/tambah`);
        }}
      />
    </>
  );
}

export default KembaliDetailTable;
