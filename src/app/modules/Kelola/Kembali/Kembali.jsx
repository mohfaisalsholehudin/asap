/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState} from "react";
import { useHistory } from "react-router-dom";
import SVG from "react-inlinesvg";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import KembaliTable from "./KembaliTable";


function Kembali() {
  const history = useHistory();
  const [tab, setTab] = useState("proses");


  // const add = () => history.push("/kelola/usulan/tambah");

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Pengembalian Aset"
          style={{ backgroundColor: "#FFC91B" }}
        >
        </CardHeader>
        <CardBody>
        <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("proses")}>
              <a
                className={`nav-link ${tab === "proses" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "proses").toString()}
              >
                Proses
              </a>
            </li>
            {/* {id && ( */}
            <>
              {" "}
              <li className="nav-item" onClick={() => setTab("monitoring")}>
                <a
                  className={`nav-link ${tab === "monitoring" && "active"}`}
                  data-toggle="tab"
                  role="button"
                  aria-selected={(tab === "monitoring").toString()}
                >
                  Monitoring
                </a>
              </li>
            </>
            {/* )} */}
          </ul>
          <div className="mt-5">
            {tab === "proses" && <KembaliTable />}
            {tab === "monitoring" && <KembaliTable tab={"monitoring"} />}
          </div>
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Kembali;
