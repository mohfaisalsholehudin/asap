import React, { useEffect, useState } from "react";
import { Modal, Table, FormControl, InputGroup, Button } from "react-bootstrap";
import SVG from "react-inlinesvg";
import * as Yup from "yup";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
// import { useSubheader } from "../../../../../../../_metronic/layout";
import { Field, Formik, Form } from "formik";
import {
  DatePickerFieldRowEnd,
  DatePickerFieldRowStart,
} from "../../../helpers/form/DatePickerField";

function KembaliTableModal({ id, show, content, onHide, saveForm }) {
  const history = useHistory();
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  //   const [title, setTitle] = useState("");

  //   useEffect(() => {
  //     let _title = id ? "Edit Peraturan Terkait" : "Tambah Peraturan Terkait";

  //     setTitle(_title);
  //     suhbeader.setTitle(_title);
  //   }, [id, suhbeader]);

  // const initialValues = {
  //   pokok_pengaturan: "",
  //   jenis: ""
  // };

  const validationSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Nama is required"),
  });

  const initValues = {
    startDate: "",
    endDate: "",
  };

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detail Pengembalian Arsip
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row mb-2">
          <div className="col-md-3 col-lg-3 col-lg-3 pl-0">
            <span className="font-weight-normal mr-2">Nama Peminjam :</span>
          </div>
          <div className="col-md-9 col-lg-9 col-lg-9">
            <span className="text-muted">Andika Perkasa</span>
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-md-3 col-lg-3 col-lg-3 pl-0">
            <span className="font-weight-normal mr-2">Tanggal Pinjam :</span>
          </div>
          <div className="col-md-9 col-lg-9 col-lg-9">
            <span className="text-muted">08-05-2023</span>
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-md-3 col-lg-3 col-lg-3 pl-0">
            <span className="font-weight-normal mr-2">Tanggal Kembali :</span>
          </div>
          <div className="col-md-9 col-lg-9 col-lg-9">
            <span className="text-muted">15-05-2023</span>
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-md-3 col-lg-3 col-lg-3 pl-0">
            <span className="font-weight-normal mr-2">Tanggal dikembalikan:</span>
          </div>
          <div className="col-md-9 col-lg-9 col-lg-9">
            <span className="text-muted">15-05-2023</span>
          </div>
        </div>
        <div className="row mb-7">
          <div className="col-md-3 col-lg-3 col-lg-3 pl-0">
            <span className="font-weight-normal mr-2">
              Lokasi Penyimpanan :
            </span>
          </div>
          <div className="col-md-9 col-lg-9 col-lg-9">
            <span className="text-muted">E-3-IV-2</span>
          </div>
        </div>
        {/* <div className="py-5">
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-normal mr-2">Nama Peminjam:</span>
              <span className="text-muted text-hover-primary">
                Andika Perkasa
              </span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-normal mr-2">Tanggal Pinjam:</span>
              <span className="text-muted">08-05-2023</span>
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
              <span className="font-weight-normal mr-2">Tanggal Kembali:</span>
              <span className="text-muted">15-05-2023</span>
            </div>
          </div> 
        </div> */}
        <div className="row">
          <Table responsive bordered hover>
            <thead
              style={{
                // border: "1px solid #3699FF",
                textAlign: "center",
              }}
            >
              <tr>
                <th>No</th>
                <th style={{ textAlign: "left" }}>Kode</th>
                <th style={{ textAlign: "left" }}>Judul Arsip</th>
                {/* <th>Aksi</th> */}
              </tr>
            </thead>
            <tbody
              style={{
                // border: "1px solid #3699FF",
                textAlign: "center",
              }}
            >
              <tr style={{ height: "40px" }}>
                <td>1</td>
                <td style={{ textAlign: "left" }}>808080919191</td>
                <td style={{ textAlign: "left" }}>
                  SPT Tahunan | Budi Eko Prasetyo
                </td>
              </tr>
              <tr style={{ height: "40px" }}>
                <td>2</td>
                <td style={{ textAlign: "left" }}>8021810109181</td>
                <td style={{ textAlign: "left" }}>
                  Pemindahbukuan | Tukul Adi Sutopo
                </td>
              </tr>
              {/* {perterkait.map((data, index) => (
                <tr key={index} style={{ height: "40px" }}>
                  <td>{index + 1}</td>
                  <td style={{ textAlign: "left" }}>{data.no_regulasi}</td>
                  <td style={{ textAlign: "left" }}>{data.perihal}</td>
                  <td>
                    <a
                      title="Tambah"
                      className="btn btn-icon btn-light btn-hover-success btn-sm mx-3"
                      onClick={() => addPer(data.id_peraturan)}
                    >
                      <span className="svg-icon svg-icon-md svg-icon-success">
                        <SVG
                          src={toAbsoluteUrl(
                            "/media/svg/icons/Navigation/Plus.svg"
                          )}
                        />
                      </span>
                    </a>
                  </td>
                </tr>
              ))} */}
            </tbody>
            {/* <tfoot style={{ border: '1px solid #3699FF', textAlign: 'center' }}>
                            <tr style={{ height: '40px' }}>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot> */}
          </Table>
        </div>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            className="btn btn-light ml-2"
            onClick={() => onHide()}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
            //   disabled={isDisabled}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Body>
    </Modal>
  );
}

export default KembaliTableModal;
