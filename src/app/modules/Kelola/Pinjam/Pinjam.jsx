/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState} from "react";
import { useHistory } from "react-router-dom";
import SVG from "react-inlinesvg";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import PinjamTable from "./PinjamTable";


function Pinjam() {
  const history = useHistory();
  const [tab, setTab] = useState("proses");


  // const add = () => history.push("/kelola/usulan/tambah");

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Peminjaman Aset"
          style={{ backgroundColor: "#FFC91B" }}
        >
          {/* <CardHeaderToolbar>
            <button
              type="button"
              className="btn btn-primary ml-2"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                float: "right",
              }}
              onClick={add}
            >
              <span className="svg-icon menu-icon">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Plus.svg")} />
              </span>
              Tambah
            </button>
          </CardHeaderToolbar> */}
        </CardHeader>
        <CardBody>
          <PinjamTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Pinjam;
