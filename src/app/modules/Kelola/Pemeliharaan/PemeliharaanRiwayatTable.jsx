/* eslint-disable react-hooks/exhaustive-deps */
/* Library */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
  toAbsoluteUrl,
} from "../../../../_metronic/_helpers";
// import { getRuangan } from "../../references/Api";

/* Utility */

function PemeliharaanRiwayatTable({ id }) {
  const history = useHistory();
  // const [content, setContent] = useState([]);
  const [data, setData] = useState([]);
  const [searchTxt, setSearchTxt] = useState("");
  const [currentPage, setCurrentPage] = useState(0);
  const [sizePage, setSizePage] = useState(10);
  const [loading, setLoading] = useState(true);

  const content = [
    {
      id: "1",
      bengkel: "Bengkel Maju Mundur",
      tglPerawatan: "20/04/2024",
      tglPerawatanSelanjutnya: "20/10/2024",
      jenisPerawatan: "Servis Berkala, Ganti Oli",
      // photo: "/media/users/user.png",
    },
    {
      id: "2",
      bengkel: "Bengkel Astra Tendean",
      tglPerawatan: "05/12/2023",
      tglPerawatanSelanjutnya: "05/06/2024",
      jenisPerawatan: "Tune Up, Ganti Ban, Ganti Oli",
      // photo: "/media/users/user.png",
    },
    {
      id: "3",
      bengkel: "Bengkel Berkat Jaya",
      tglPerawatan: "12/09/2023",
      tglPerawatanSelanjutnya: "12/04/2024",
      jenisPerawatan: "Servis Berkala, Ganti Aki, Servis AC",
      // photo: "/media/users/user.png",
    },
    {
      id: "4",
      bengkel: "Bengkel Arthomoro",
      tglPerawatan: "10/08/2023",
      tglPerawatanSelanjutnya: "10/04/2024",
      jenisPerawatan: "Servis Berkala, Balancing dan Spooring Ban",
      // photo: "/media/users/user.png",
    },
  ];

  const add = () => history.push(`/kelola/pemeliharaan/${id}/riwayat/tambah`);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      hidden: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber =
          (currentPage === 0 ? currentPage + 1 - 1 : currentPage - 1) *
            sizePage +
          (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      // style: {
      //   width: "100px",
      // },
    },
    {
      dataField: "id",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "bengkel",
      text: "Nama Bengkel",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // style: {
      //   width: "10px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },

    {
      dataField: "tglPerawatan",
      text: "Tanggal Perawatan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // style: {
      //   width: "100px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "tglPerawatanSelanjutnya",
      text: "Tanggal Perawatan Selanjutnya",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // style: {
      //   width: "100px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    {
      dataField: "jenisPerawatan",
      text: "Jenis Perawatan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      // style: {
      //   width: "100px",
      // },
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
    },
    // {
    //   dataField: "status",
    //   text: "Status",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   hidden: tab === 'selesai' || tab === 'tolak' ? true : false,
    //   formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeUsulanMatrix,
    // },
    // {
    //   dataField: "action",
    //   text: "Aksi",
    //   formatter:
    //     columnFormatters.ActionsColumnFormatterDataArsip,
    //   formatExtraData: {
    //     openEditDialog: edit,
    //     openDetailDialog: view
    //     // openDeleteDialog: deleteAction,
    //     // publishKnowledge: apply,
    //     // showReview: review,
    //   },
    //   classes: "text-center pr-0",
    //   headerClasses: "text-center pr-3",
    // },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id",
    pageNumber:
      currentPage === 0
        ? currentPage + 1
        : currentPage === 1
        ? currentPage
        : currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "id", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: data.totalElements,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  // const emptyDataMessage = () => {
  //   return "Tidak Ada Data";
  // };
  const { SearchBar } = Search;
  const handleTableChange = (
    type,
    { sortField, sortOrder, data, filters, ...props }
  ) => {};
  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                            placeholder="Cari"
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        // remote
                        // onTableChange={handleTableChange}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination
                        paginationProps={paginationProps}
                        // isLoading={loading}
                      />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default PemeliharaanRiwayatTable;
