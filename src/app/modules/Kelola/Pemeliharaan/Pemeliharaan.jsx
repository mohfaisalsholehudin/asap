/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState} from "react";
import { useHistory } from "react-router-dom";
import SVG from "react-inlinesvg";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import PemeliharaanTable from "./PemeliharaanTable";


function Pemeliharaan() {
  const history = useHistory();
  const [tab, setTab] = useState("proses");


  // const add = () => history.push("/kelola/usulan/tambah");

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Pemeliharaan Aset"
          style={{ backgroundColor: "#FFC91B" }}
        >
        </CardHeader>
        <CardBody>
          <PemeliharaanTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Pemeliharaan;
