import React, { useState } from "react";
import { Field, Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import {
  Input,
  Select as Sel,
  Textarea,
  DatePickerField,
  Checkbox,
} from "../../../helpers";
import { CustomUploadPhoto } from "../../../helpers/form/CustomUploadPhoto";
import { DatePickerFieldTanggal } from "../../../../_metronic/_partials/controls/forms/DatePickerField";

function PemeliharaanForm({ content, btnRef, saveForm }) {
  const data = [
    {
      id: "1",
      nama: "Servis Berkala",
    },
    {
      id: "2",
      nama: "Tune Up",
    },
    {
      id: "3",
      nama: "Ganti Oli",
    },
    {
      id: "4",
      nama: "Ganti Aki",
    },
    {
      id: "5",
      nama: "Balancing dan Spooring Ban",
    },
    {
      id: "6",
      nama: "Servis AC",
    },
    {
      id: "7",
      nama: "Ganti Ban",
    },
  ];

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    jenisPerawatan: Yup.string().required("Jenis wajib diisi"),
    tglPerawatan: Yup.string().required("Tanggal Perawatan wajib diisi"),
    tglPerawatanSelanjutnya: Yup.string().required(
      "Tanggal Perawatan wajib diisi"
    ),
    bengkel: Yup.string().required("No. Plat wajib diisi"),
    alamat: Yup.string().required("Deskripsi wajib diisi"),
    file: Yup.mixed()
      .required("File wajib diisi")
      .test(
        "fileSize",
        "File terlalu besar",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Format file tidak sesuai",
        (value) =>
          value && SUPPORTED_FORMATS.some((a) => value.type.includes(a))
      ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["image/jpeg"];

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
        }}
      >
        {({ handleSubmit }) => {
          // const options = [];
          // for (let i = minOffset; i <= maxOffset; i++) {
          //   const year = thisYear - i;
          //   options.push(
          //     <option key={year} value={year}>
          //       {year}
          //     </option>
          //   );
          // }
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Bengkel */}
                <div className="form-group row">
                  <Field
                    name="bengkel"
                    component={Input}
                    placeholder="Nama Bengkel"
                    label="Nama Bengkel"
                  />
                </div>
                {/* Field Alamat */}
                <div className="form-group row">
                  <Field
                    name="alamat"
                    component={Textarea}
                    placeholder="Alamat Bengkel"
                    label="Alamat Bengkel"
                  />
                </div>
                {/* Field Tanggal Perawatan */}
                <div className="form-group row">
                  <DatePickerField
                    name="tglPerawatan"
                    label="Tanggal Perawatan"
                  />
                </div>
                {/* Field Tanggal Perawatan Selanjutnya*/}
                <div className="form-group row">
                  <DatePickerField
                    name="tglPerawatanSelanjutnya"
                    label="Tanggal Perawatan Selanjutnya"
                  />
                </div>
                {/* Field Jenis Perawatan */}

                <div className="form-group row align-items-center">
                <label className="col-xl-3 col-lg-3 col-form-label">
                    Pilih Jenis Perawatan
                  </label>
                  <div className="col-lg-9 col-xl-9">
                    <div className="checkbox-inline">
                      {data.map((data, index) => (
                        <Field
                          component={Checkbox}
                          name="jenisPerawatan"
                          type="checkbox"
                          value={data.nama}
                          content={data.nama}
                          key={index}
                        />
                      ))}
                    </div>
                  </div>
                </div>

                {/* Field Unggah Bukti Perawatan */}

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unggah Bukti Perawatan
                  </label>
                  <div className="col-lg-9 col-xl-9">
                    <Field
                      name="file"
                      component={CustomUploadPhoto}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PemeliharaanForm;
