/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import axios from "axios";
import { MenuOpenOutlined } from "@material-ui/icons";

export function AsideMenuList({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu &&
          "menu-item-active"} menu-item-open menu-item-not-hightlighted`
      : "";
  };

  const { REACT_APP_MENU_URL } = process.env;
  const [menu, setMenu] = useState([]);

  const getMenu = async () => {
    try {
      const data = {
        role: ["99", "8"],
        route: "dashboard",
      };
      const response = await axios.post(`${REACT_APP_MENU_URL}`, data);
      return response;
    } catch (err) {
      if (err.response) {
        console.log(err.response);
      } else {
        console.log(err);
      }
    }
  };

  useEffect(() => {
    // getMenu()
    // .then(({data: {data}})=> {
    //   setMenu(data.menu);
    //   // console.log(data.menu);
    //   Object.values(data.menu).map((data)=>{
    //     console.log(data[1]);
    //   })
    // })
  }, []);

  // const showMenu = () => {
  //    menu.map((data, index)=>{
  //       console.log(index);
  //   })
  // }

  return (
    <>
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/* {showMenu()} */}
        {/* Dashboard Pages */}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Devices/Display1.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>
        {/* End of Dashboard Pages */}

        {/* Master Pages */}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/master",
            true
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/master">
            <span className="svg-icon menu-icon">
              {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Settings4.svg")} /> */}
              <i className="fas fa-database"></i>

            </span>
            <span className="menu-text">Master</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Master</span>
                </span>
              </li>
              <li
                className={`menu-item ${getMenuItemActive("/master/pengguna")}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/master/pengguna"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Pengguna</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive("/master/ruangan")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/master/ruangan">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Ruangan</span>
                </NavLink>
              </li>
              {/* <li
                className={`menu-item ${getMenuItemActive("/master/atk")}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/master/atk"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">ATK</span>
                </NavLink>
              </li> */}
              <li
                className={`menu-item menu-item-submenu ${getMenuItemActive(
                  "/master/atk",
                  true
                )}`}
                aria-haspopup="true"
                data-menu-toggle="hover"
              >
                <NavLink className="menu-link menu-toggle" to="/master/atk">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">ATK</span>
                  <i className="menu-arrow" />
                </NavLink>
                <div className="menu-submenu ">
                  <i className="menu-arrow" />
                  <ul className="menu-subnav">
                    <li
                      className="menu-item  menu-item-parent"
                      aria-haspopup="true"
                    >
                      <span className="menu-link">
                        <span className="menu-text">ATK</span>
                      </span>
                    </li>
                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/master/atk/list"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/master/atk/list"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">Daftar ATK</span>
                      </NavLink>
                    </li>
                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/master/atk/pembelian"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/master/atk/pembelian"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">
                          Pembelian / Pencarian ATK
                        </span>
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </li>
              <li
                className={`menu-item ${getMenuItemActive("/master/kdo")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/master/kdo">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">KDO</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive("/master/bmn")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/master/bmn">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">BMN</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </li>
        {/* End of Master Pages */}

        {/* Data Arsip Pages */}
        <li
          className={`menu-item ${getMenuItemActive("/arsip", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/arsip">
            <span className="svg-icon menu-icon">
              {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} /> */}
              <i className="far fa-file-alt"></i>
            </span>
            <span className="menu-text">Data Arsip</span>
          </NavLink>
        </li>
        {/* End of Data Arsip Pages */}

        {/* Kelola Arsip Pages */}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/kelola",
            true
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/kelola">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl("/media/svg/icons/Communication/Group.svg")}
              /> */}
              <i className="fas fa-retweet"></i>
            </span>
            <span className="menu-text">Kelola Aset</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Kelola Aset</span>
                </span>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/kelola/usulan"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/kelola/usulan"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Peminjaman</span>
                </NavLink>
              </li>
              {/* <li
                className={`menu-item ${getMenuItemActive(
                  "/kelola/peminjaman"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/kelola/peminjaman"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Peminjaman</span>
                </NavLink>
              </li> */}
              <li
                className={`menu-item ${getMenuItemActive(
                  "/kelola/pengembalian"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/kelola/pengembalian"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Pengembalian</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/kelola/pemeliharaan"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/kelola/pemeliharaan"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Pemeliharaan</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </li>
        {/* End of Kelola Arsip Pages */}

        {/* Administrator Pages */}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/admin",
            true
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/admin">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl("/media/svg/icons/Communication/Group.svg")}
              /> */}
              <i className="fas fa-users-cog"></i>
            </span>
            <span className="menu-text">Administrator</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Administrator</span>
                </span>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/admin/pengaturan"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/admin/pengaturan"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Pengaturan</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/admin/tipe-arsip"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/admin/tipe-arsip"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Tipe Arsip</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/admin/lokasi"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/admin/lokasi"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Lokasi Penyimpanan</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/admin/pemeliharaan-jenis"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/admin/pemeliharaan-jenis"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Jenis Pemeliharaan</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </li>
        {/* End ofAdministrator Pages */}

      </ul>
    </>
  );
}
