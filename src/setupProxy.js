const { createProxyMiddleware } = require('http-proxy-middleware');
// const {BACKEND_URL} = window.ENV;

module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: "http://10.244.1.140:8888",
      changeOrigin: true,
    })
  );
};